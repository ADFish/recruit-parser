# Recruit Data and Parser

## Recruit Data Configuration
Recruit Data is saved in following path now:
`/records/datasets/recruit2016`

The file configuration is as follow:

```
|--RawDataFromRecruit_ver201701
|  |--RawDataFromRecruit
|  |  |--20151211_GroupDiscussion
|  |  |--isp_preprocess
|  |  |--20160122_code4analysis_isp
|  |  |  |--staff_data
|  |  |  |  |--KPIdata_fix_CA.csv
|  |  |  |  |--KPIdata_fix_RA.csv
|  |  |  |  |--active_times.csv
|  |  |  |  |--staff_role.csv
|  |  |--Raw
|  |  |  |--2850_001
|  |  |  |  |--storage_slow
|  |  |  |  |  |--2850_accel_storage_20151021T202430_13-6.txt
|  |  |  |  |  ...
|  |  |  |  |  |--2850_audioamp_storage_20151021T202430_13-2.txt
|  |  |  |  |  ...
|  |  |  |  |  |--2850_bluetooth_storage_20151021T202430_13-1.txt
|  |  |  |  |  ...
|  |  |  |  |  |--2850_infrared_storage_20151021T202430_13-4.txt
|  |  |  |  |  ...
|  |  |  |  |  |--2850_log_storage_20151021T202430_13-8.txt
|  |  |  |  |  ...
|  |  |  |  |  |--2850_rtc_storage_20151021T202430_13-5.txt
|  |  |  |  |  ...
|  |  |  |  |  |--2850_usbpower_storage_20151021T202430_13-7.txt
|  |  |  ...
|  |  |  |--2988_080
|  |  |  |--Data_Osaka_all1.zip
|  |  |  |--Data_Osaka_all2.zip
|  |  |  |--Data_Osaka_all3.zip
|  |  |  |--Data_Osaka_all4.zip
|  |  |  |--Data_Osaka_all5.zip
|  |--readme.md
|--zz-old
|  |--data20160600
|  |  |--OtherDataFromRecruit
|  |  |  |--KPIdata_update20160117.xlsx
|  |  |  |--MetaData_Osaka.xlsx
|  |  |  |--readme.txt
|  |  |  |--sample_2851
|  |  |--RelationshipDataFromRecruit
|  |--data-all-ver201701
|  |--data201608
|  |--data201701
|--unclassified
|  |--20170406-morooka-to-simosaka-recruitdata-complete
|  |  |--recruit2016
|  |  |  |--RawData-all-ver201701
|  |  |  |--data201701_lackedData
|  |  |  |--data201606_HumanizePreprocessedData
|  |  |  |--zz-old
|  |  |  |--README.md
|--readme.md
|--kpi_attribute_data
|  |--KPIdata_fix_CA.csv
|  |--KPIdata_fix_RA.csv
|  |--MetaData_Osaka_exported.csv
```

Note: `kpi_attribute_data` directory is added in 2018/05/11 by Duan. This directory is used for gathering distributed users' attributes files into one directory for easy use.
In the `kpi_attribute_data` directory, the original `KPIdata_fix_CA.csv` and `KPIdata_fix_RA.csv` can be found in `RawDataFromRecruit_ver201701/RawDataFromRecruit/20160122_code4analysis_isp/staff_data/`;
And the `MetaData_Osaka_exported.csv` is exported from `zz-old/data20160600/OtherDataFromRecruit/etaData_Osaka.xlsx`.


The raw sensor data is in `RawDataFromRecruit_ver201701/RawDataFromRecruit/Raw/`

## Role Parser
### Data description
Role parser is for `MetaData_Osaka_exported.csv`. This is a file describe user's properties, it has following attributes in every row:
```
Personal ID: [Int] This number is the alias for each user
Participation: [String] This string indicates the user is participants or not. There is three types of this string: Participant, non-Participation, no allocation
Badge ID: [Int] This number is the device number
Data Directory: [String] This string indicates the Directory Name of each user for saving raw data in "Raw" directory metioned in the configuration part. 
Team: [String] There are five types of teams: RA_2G, RA_3G, CA_3G, CA_4G(CA_4G, CA_4G_H, CA_4G_I, CA_4G_K, CA_4G_O), other
Job Type: [Int] There are 4 types of Jobs: 1, permanent employee; 2, employee under limited-term contract; 3, temporary worker from a agency; 4, others
Job Role: [String] There are 4 types of role: Sales Staff, General affairs, General Mgr, Staff for a specific project
KPI Data: [String] There are 2 types: w; w/o
```
A typical example of the data is as follow: 
```
Personal ID,Participation,Badge ID,Data Directory,Team,Job Type,Job Role,KPI Data
1,Participation,2850,2850_001,RA_2G,1,Sales Staff ,w
```

### Parser description
- Example: RoleLoaderExample.scala
- Row data parser:  RoleParser.scala 
- Case class for RoleParser: RoleData.scala
- Some class that includes utility methods:
    - JobType.scala
    - JobRole.scala
    - JobGroup.scala
    - FileGetter.scala
- A utility class contains all methods and class needed for participants property for easy use: ParticipantsPropertyCollector.class
- Test code:
    - For RoleParser: RoleParserTest.scala
    - For ParticipantsPropertyCollector: ParticipantsPropertyCollector.scala
 