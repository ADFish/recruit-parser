/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ParticipantsPropertyCollectorTest extends FunSuite {
  val session = startUpSpark()

  val path = "testData/recruit/MetaData_Osaka_exported.csv"
  val fileRootPath = "testData/recruit/FileExample"

  val roleParse = new RoleParser
  val roleDataset = roleParse.loadDataset(session, path)

  val participantsProperty = new ParticipantsPropertyCollector(roleDataset, fileRootPath)

  test("Test ParticipantsPropertyCollector class") {
    val user: Int = 2850
    val date = "20151027"

    assert(participantsProperty.participantsRoleDataset.count == 59)
    assert(participantsProperty.participantsIds.length == 59)
    assert(participantsProperty.participantsIds(0) == user)

    assert(participantsProperty.isParticipants(user) == true)
    assert(participantsProperty.getParticipantsAlias(user) == 1)
    assert(participantsProperty.fileGetter.getDirectoryName(user) == "2850_001")
    assert(participantsProperty.hasKPIData(user) == true)

    //Test fileGetter class
    assert(participantsProperty.fileGetter.getRootPath(user) ==
      "testData/recruit/FileExample/2850_001/storage_slow")
    assert(participantsProperty.fileGetter.getAccelFiles(user).mkString(" ") ==
      "testData/recruit/FileExample/2850_001/storage_slow/2850_accel_storage_20151027*"
    )
    assert(participantsProperty.fileGetter.getAccelFile(user, date) ==
      "testData/recruit/FileExample/2850_001/storage_slow/2850_accel_storage_20151027*"
    )
    assert(participantsProperty.fileGetter.getInfraredFiles(user).mkString(" ") ==
      "testData/recruit/FileExample/2850_001/storage_slow/2850_infrared_storage_20151027*"
    )

    assert(participantsProperty.fileGetter.getInfraredFile(user, date) ==
      "testData/recruit/FileExample/2850_001/storage_slow/2850_infrared_storage_20151027*"
    )
    assert(participantsProperty.fileGetter.getBluetoothFiles(user).mkString(" ") ==
      "testData/recruit/FileExample/2850_001/storage_slow/2850_bluetooth_storage_20151027*"
    )
    assert(participantsProperty.fileGetter.getBluetoothFile(user, date) ==
      "testData/recruit/FileExample/2850_001/storage_slow/2850_bluetooth_storage_20151027*"
    )

    //Test jobGroup class
    assert(participantsProperty.jobGroup.teamRA2.toArray.sorted.mkString(", ")
      == "2850, 2851, 2852, 2853, 2858, 2859, 2860, 2895, 2950, 2977, 2978, 2980, 2981"
    )
    assert(participantsProperty.jobGroup.teamRA3.toArray.sorted.mkString(", ")
      == "2861, 2866, 2867, 2870, 2964, 2969, 2970, 2971, 2973, 2974, 2976"
    )
    assert(participantsProperty.jobGroup.teamCA3.toArray.sorted.mkString(", ")
      == "2871, 2872, 2873, 2874, 2875, 2876, 2935, 2938, 2955, 2956, 2957, 2958, 2960"
    )
    assert(participantsProperty.jobGroup.teamCA4.toArray.sorted.mkString(", ")
      == "2877, 2888, 2889, 2890, 2891, 2892, 2896, 2897, 2899, 2910, 2931, 2932, 2934, 2936, 2937, 2951, 2952, 2953, 2954"
    )
    assert(participantsProperty.jobGroup.getTeam(user) == "RA_2G")

    //Test jobType class
    assert(participantsProperty.jobType.getJobTypeName(user) == "permanent employee")
    assert(participantsProperty.jobType.temporaryEmployees.toArray.sorted.mkString(", ") ==
      "2859, 2860, 2870, 2938, 2950, 2955, 2956, 2964, 2969, 2970, 2977"
    )
    assert(participantsProperty.jobType.otherEmployees.mkString(" ") ==
      "2939"
    )

    //Test jobRole class
    assert(participantsProperty.jobRole.getJobRole(user) == "Sales Staff")
    assert(participantsProperty.jobRole.managerEmployees.toArray.sorted.mkString(", ")
      == "2877, 2893"
    )
    assert(participantsProperty.jobRole.affairEmployees.toArray.sorted.mkString(", ")
      == "2852, 2894, 2957, 2971"
    )
    assert(participantsProperty.jobRole.otherEmployees.mkString(" ")
      == "2939"
    )
  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("roleParser")
      .getOrCreate
  }
}
