/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by duan on 2018/06/22
  */
@RunWith(classOf[JUnitRunner])
class RoleParserTest extends FunSuite {
  val session = startUpSpark()

  val path = "testData/recruit/MetaData_Osaka_exported.csv"
  val fileRootPath = "testData/recruit/FileExample"

  val roleParse = new RoleParser
  val roleDataset = roleParse.loadDataset(session, path)

  test("Test role parser: load dataset function") {
    val size = roleDataset.count().toInt
    assert(size == 82)

    val user1 = roleDataset.first()
    val user1PersonalId = user1.personalId
    assert(user1PersonalId == 1)
    val user1Participation = user1.participation
    assert(user1Participation == "Participation")
    val user1Id = user1.badgeId
    assert(user1Id == 2850)
    val user1DataDir = user1.dataDirectory
    assert(user1DataDir == "2850_001")
    val user1Team = user1.team
    assert(user1Team == "RA_2G")
    val user1JobType = user1.jobType
    assert(user1JobType == 1)
    val user1JobRole = user1.jobRole
    assert(user1JobRole == "Sales Staff ")
    val user1HasKPI = user1.KPIData
    assert(user1HasKPI == "w")

    val user2 = roleDataset.take(size)(size - 1)
    val user2PersonalId = user2.personalId
    assert(user2PersonalId == 82)
    val user2Participation = user2.participation
    assert(user2Participation == "non-Participation")
    val user2Id = user2.badgeId
    assert(user2Id == 0)
    val user2DataDir = user2.dataDirectory
    assert(user2DataDir == null)
    val user2Team = user2.team
    assert(user2Team == "CA_4G_O")
    val user2JobType = user2.jobType
    assert(user2JobType == 2)
    val user2JobRole = user2.jobRole
    assert(user2JobRole == "Sales Staff ")
    val user2HasKPI = user2.KPIData
    assert(user2HasKPI == "w")
  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("roleParser")
      .getOrCreate
  }

}
