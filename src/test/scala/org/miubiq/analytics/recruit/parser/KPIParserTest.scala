/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.junit.runner.RunWith
import org.miubiq.commons.io.MiubiqConf
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by duan on 2018/07/20
  */
@RunWith(classOf[JUnitRunner])
class KPIParserTest extends FunSuite {

  val session = startUpSpark()

  val caFilePath = "testData/recruit/KPIdata_fix_CA.csv"
  val raFilePath = "testData/recruit/KPIdata_fix_RA.csv"
  val path = "testData/recruit/MetaData_Osaka_exported.csv"
  val dataPath = MiubiqConf.inPath("recruit2016/RawDataFromRecruit_ver201701/RawDataFromRecruit/Raw")

  val roleParse = new RoleParser
  val roleDataset = roleParse.loadDataset(session, path)

  val metaData = new ParticipantsPropertyCollector(roleDataset, dataPath)

  val kpiParser = new KPIParser

  val caRawDataset = kpiParser.loadCARawDataset(session, caFilePath)
  val raRawDataset = kpiParser.loadRARawDataset(session, raFilePath)

  val caDataset = kpiParser.loadCADataset(session, caFilePath)
  val raDataset = kpiParser.loadRADataset(session, raFilePath)

  test("Test loader for ca and ra team raw dataset") {

    //職位ID,部署ID,デバイスID,期間_FROM,期間_TO,受付_人数,初回面談_人数,登録_人数,資料送付_件数,資料送付_人数,CA推薦_件数,CA推薦_人数,RAOUT_件数,RAOUT_人数,書類OUT_件数,書類OUT_人数,書類OK_件数,企業推薦-書類OK件数率,書類OK_人数,一面実施_件数,一面実施_人数,一面OUT_件数,一面OUT_人数,一面OK_件数,一面OK_人数,企業内定_件数,企業内定_人数,決定_人数,KPI_CA_1(CA推薦_件数 - RAOUT_件数),KPI_CA_2(( CA推薦_件数 - RAOUT_件数 ) /CA推薦_件数),KPI_CA_3(CA推薦_人数-RAOUT_人数),KPI_CA_4(( CA推薦_人数-RAOUT_人数 ) /CA推薦_人数),KPI_CA_5(企業内定_件数),KPI_CA_6(企業内定_人数),KPI_CA_7(決定_人数)
    //Raw data: 1,CA_3G,2876,2015/10/5,2015/10/11,0,0,0,1 ,1,0,0,0,0,0,0,0,−,0,0,0,0,0,0,0,0,0,0,0,0.000 ,0,0.000 ,0 ,0 ,0
    val firstTest = caRawDataset.first()
    assert(firstTest.jobType == 1)
    assert(firstTest.badgeId == 2876)
    assert(firstTest.team == "CA_3G")
    assert(firstTest.beginDate == "2015/10/5")
    assert(firstTest.endDate == "2015/10/11")
    assert(firstTest.kpiCA1 == 0)
    assert(firstTest.kpiCA2 == 0.0)
    assert(firstTest.kpiCA3 == 0)
    assert(firstTest.kpiCA4 == 0.0)
    assert(firstTest.kpiCA5 == 0.0)
    assert(firstTest.kpiCA6 == 0.0)
    assert(firstTest.kpiCA7 == 0)
    assert(firstTest.caReferralItem == 0)
    assert(firstTest.caReferralHeadcount == 0)

    //Raw data: 2,CA_4G_K,2936,2015/9/28,2015/10/4,0,0,0,1 ,1,0,0,0,0,0,0,0,−,0,0,0,0,0,0,0,0,0,0,0,0.000 ,0,0.000 ,0 ,0 ,0
    val secondTest = caRawDataset.take(3)(2)
    assert(secondTest.jobType == 2)
    assert(secondTest.badgeId == 2936)
    assert(secondTest.team == "CA_4G_K")
    assert(secondTest.beginDate == "2015/9/28")
    assert(secondTest.endDate == "2015/10/4")
    assert(secondTest.kpiCA1 == 0)
    assert(secondTest.kpiCA2 == 0.0)
    assert(secondTest.kpiCA3 == 0)
    assert(secondTest.kpiCA4 == 0.0)
    assert(secondTest.kpiCA5 == 0.0)
    assert(secondTest.kpiCA6 == 0.0)
    assert(secondTest.kpiCA7 == 0)
    assert(secondTest.caReferralItem == 0)
    assert(secondTest.caReferralHeadcount == 0)

    //Raw data: 1,RA_2G,2981,2015/9/28,2015/10/4,0,0,1,6,2,124,2822 ,51,114,38,70,30,53,12,16,21,37,13,33,7,9,2,2,0,0,0,3,14,15,0,6,44,0.386 ,13 ,0.255 ,0,15
    val thirdTest = raRawDataset.first()
    assert(thirdTest.jobType == 1)
    assert(thirdTest.badgeId == 2981)
    assert(thirdTest.team == "RA_2G")
    assert(thirdTest.beginDate == "2015/9/28")
    assert(thirdTest.endDate == "2015/10/4")
    assert(thirdTest.kpiRA1 == 0)
    assert(thirdTest.kpiRA2 == 6)
    assert(thirdTest.kpiRA3 == 44)
    assert(thirdTest.kpiRA4 == 0.386)
    assert(thirdTest.kpiRA5 == 13.0)
    assert(thirdTest.kpiRA6 == 0.255)
    assert(thirdTest.kpiRA7 == 0)
    assert(thirdTest.kpiRA8 == 15)
    assert(thirdTest.caReferralItem == 114)
    assert(thirdTest.caReferralHeadcount == 51)
    assert(thirdTest.informalDecisionHeadcount == 0)

    //Raw data: 1,RA_2G,2853,2015/9/28,2015/10/4,0,0,0,0,0,65,1688 ,22,35,7,7,23,40,9,12,17,36,9,12,11,11,1,1,0,0,0,2,2,2,0,0,28,0.800 ,15 ,0.682 ,0,2
    val fourthTest = raRawDataset.take(11)(10)
    assert(fourthTest.jobType == 1)
    assert(fourthTest.badgeId == 2853)
    assert(fourthTest.team == "RA_2G")
    assert(fourthTest.beginDate == "2015/9/28")
    assert(fourthTest.endDate == "2015/10/4")
    assert(fourthTest.kpiRA1 == 0)
    assert(fourthTest.kpiRA2 == 0)
    assert(fourthTest.kpiRA3 == 28)
    assert(fourthTest.kpiRA4 == 0.8)
    assert(fourthTest.kpiRA5 == 15.0)
    assert(fourthTest.kpiRA6 == 0.682)
    assert(fourthTest.kpiRA7 == 0)
    assert(fourthTest.kpiRA8 == 2)
    assert(fourthTest.caReferralItem == 35)
    assert(fourthTest.caReferralHeadcount == 22)
    assert(fourthTest.informalDecisionHeadcount == 0)
  }

  test("Test loader for ca and ra team dataset (Sum of kpi from 20150928 ~ 20151206)") {
    /*
    Raw data of user 2871
        1,CA_3G,2871,2015/9/28,2015/10/4,4,6,5,1346 ,111,67,12,31,10,14,10,15,62.5%,12,14,10,8,8,3,2,6,4,8,36,0.537 ,2,0.167 ,6 ,4 ,8
        1,CA_3G,2871,2015/10/5,2015/10/11,7,5,6,1251 ,114,68,15,56,14,15,8,9,29.0%,7,13,9,3,3,5,5,3,3,0,12,0.176 ,1,0.067 ,3 ,3 ,0
        1,CA_3G,2871,2015/10/12,2015/10/18,10,11,6,1273 ,121,46,13,30,7,16,10,3,23.1%,3,5,5,3,3,5,5,1,1,0,16,0.348 ,6,0.462 ,1 ,1 ,0
        1,CA_3G,2871,2015/10/19,2015/10/25,12,17,10,1570 ,147,69,19,30,12,16,12,12,35.3%,9,5,4,3,2,1,1,2,2,2,39,0.565 ,7,0.368 ,2 ,2 ,2
        1,CA_3G,2871,2015/10/26,2015/11/1,3,6,7,1491 ,133,99,22,36,9,16,10,13,33.3%,8,9,8,4,4,3,3,3,3,4,63,0.636 ,13,0.591 ,3 ,3 ,4
        1,CA_3G,2871,2015/11/2,2015/11/8,6,9,8,1558 ,121,43,14,42,8,12,11,13,76.5%,11,11,9,3,3,6,6,0,0,0,1,0.023 ,6,0.429 ,0 ,0 ,0
        1,CA_3G,2871,2015/11/9,2015/11/15,8,7,3,1325 ,146,38,13,22,9,16,12,6,21.4%,6,8,7,4,3,4,3,3,3,0,16,0.421 ,4,0.308 ,3 ,3 ,0
        1,CA_3G,2871,2015/11/16,2015/11/22,5,10,8,1486 ,141,47,14,16,8,6,6,8,42.1%,6,10,10,1,1,2,2,3,3,0,31,0.660 ,6,0.429 ,3 ,3 ,0
        1,CA_3G,2871,2015/11/23,2015/11/29,1,2,3,1176 ,102,62,25,25,12,14,12,10,41.7%,7,4,4,2,2,3,3,2,2,4,37,0.597 ,13,0.520 ,2 ,2 ,4
        1,CA_3G,2871,2015/11/30,2015/12/6,7,7,5,1065 ,105,29,11,17,11,18,11,10,34.5%,8,3,3,3,3,3,3,3,3,5,12,0.414 ,0,0.000 ,3 ,3 ,5
        1,CA_3G,2871,2015/10/5,2015/10/11,1,0,0,11 ,1,0,0,0,0,0,0,0,−,0,0,0,0,0,0,0,0,0,0,0,0.000 ,0,0.000 ,0 ,0 ,0
        1,CA_3G,2871,2015/11/16,2015/11/22,1,1,1,34 ,1,0,0,0,0,0,0,0,−,0,0,0,0,0,0,0,0,0,0,0,0.000 ,0,0.000 ,0 ,0 ,0
        */
    val test1 = caDataset.sort("badgeId").first()
    assert(test1.jobType == 1)
    assert(test1.badgeId == 2871)
    assert(test1.team == "CA_3G")
    assert(test1.beginDate == "20150928")
    assert(test1.endDate == "20151206")
    assert(test1.kpiCA1 == 263.0)
    assert(test1.kpiCA3 == 58.0)
    assert(test1.kpiCA5 == 26.0)
    assert(test1.kpiCA6 == 24.0)
    assert(test1.kpiCA7 == 23.0)

    /*
    Raw data of user 2850
        1,RA_2G,2850,2015/9/28,2015/10/4,0,0,3,4,10,111,1313 ,26,58,12,25,16,41,11,37,6,9,8,8,4,5,4,5,1,2,2,2,4,5,0,4,33,0.569 ,14 ,0.538 ,2,5
        1,RA_2G,2850,2015/10/5,2015/10/11,0,0,4,18,2,114,1318 ,32,60,13,25,30,50,14,29,9,13,8,9,1,1,2,2,2,2,2,0,0,0,0,18,35,0.583 ,19 ,0.594 ,2,0
        1,RA_2G,2850,2015/10/12,2015/10/18,0,0,4,4,1,110,1497 ,29,52,14,28,21,24,19,29,8,14,9,12,5,7,5,6,0,0,0,0,0,0,0,4,24,0.462 ,15 ,0.517 ,0,0
        1,RA_2G,2850,2015/10/19,2015/10/25,0,0,7,11,9,136,1928 ,42,91,22,44,16,22,8,9,7,7,5,7,2,2,3,3,3,3,3,0,0,0,0,11,47,0.516 ,20 ,0.476 ,3,0
        1,RA_2G,2850,2015/10/26,2015/11/1,0,0,7,13,2,132,2194 ,42,97,22,54,29,49,15,25,12,19,8,11,4,4,3,3,1,1,1,4,5,5,0,13,43,0.443 ,20 ,0.476 ,1,5
        1,RA_2G,2850,2015/11/2,2015/11/8,0,0,5,6,1,114,1293 ,29,66,15,32,28,41,20,24,11,16,6,6,5,5,0,0,1,1,1,0,0,0,0,6,34,0.515 ,14 ,0.483 ,1,0
        1,RA_2G,2850,2015/11/9,2015/11/15,0,0,7,24,8,133,2396 ,41,102,20,32,30,49,12,16,18,27,8,10,2,2,3,3,1,1,1,0,0,0,0,24,70,0.686 ,21 ,0.512 ,1,0
        1,RA_2G,2850,2015/11/16,2015/11/22,0,0,0,0,0,113,1704 ,36,82,21,48,31,61,4,5,7,13,14,21,4,4,7,8,1,1,1,1,1,1,0,0,34,0.415 ,15 ,0.417 ,1,1
        1,RA_2G,2850,2015/11/23,2015/11/29,0,0,6,11,5,116,1841 ,40,94,20,32,24,28,25,49,19,19,9,14,5,7,5,6,0,0,0,1,1,1,0,11,62,0.660 ,20 ,0.500 ,0,1
        1,RA_2G,2850,2015/11/30,2015/12/6,0,0,4,5,9,125,3782 ,38,119,23,69,25,68,20,32,13,32,13,14,7,7,1,1,2,2,2,2,2,2,0,5,50,0.420 ,15 ,0.395 ,2,2
        */
    val test2 = raDataset.sort("badgeId").first()
    assert(test2.jobType == 1)
    assert(test2.badgeId == 2850)
    assert(test2.team == "RA_2G")
    assert(test2.beginDate == "20150928")
    assert(test2.endDate == "20151206")
    assert(test2.kpiRA1 == 432.0)
    assert(test2.kpiRA3 == 173.0)
    assert(test2.kpiRA5 == 13.0)
    assert(test2.kpiRA6 == 13.0)
    assert(test2.kpiRA7 == 14.0)
    assert(test2.kpiRA8 == 0.0)
    assert(test2.kpiRA9 == 96.0)
  }

  test("Get KPI function in ParticipantPropertyCollector") {
    val user = 2871
    assert(metaData.getKPI(user, caDataset, raDataset, 1) == 263.0)
    assert(metaData.getKPI(user, caDataset, raDataset, 3) == 58.0)
    assert(metaData.getKPI(user, caDataset, raDataset, 5) == 26.0)
    assert(metaData.getKPI(user, caDataset, raDataset, 6) == 24.0)
    assert(metaData.getKPI(user, caDataset, raDataset, 7) == 23.0)
  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("kpiParserCA")
      .getOrCreate
  }
}
