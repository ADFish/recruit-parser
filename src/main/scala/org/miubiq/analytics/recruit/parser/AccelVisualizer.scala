/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import org.apache.spark.sql.Dataset
import org.miubiq.commons.mwplot.Plotter._

/**
  * Created by duan on 12/17/17.
  */
object AccelVisualizer {

  /**
    * This method shows the figure of raw accelerometer dataset
    * @param accelDataset The raw accelerometer dataset
    * @return Show the figure of raw accelerometer dataset.
    */
  def view(accelDataset: Dataset[AccelData]): Unit = {
    val tsData = accelDataset.sort("unixTimeStamp").collect
    val n = tsData.length

    val t = new Array[Long](n)
    val x = new Array[Double](n)
    val y = new Array[Double](n)
    val z = new Array[Double](n)

    for (i <- 0 until n) {
      t(i) = tsData(i).unixTimeStamp
      x(i) = tsData(i).x
      y(i) = tsData(i).y
      z(i) = tsData(i).z
    }

    val cal = Calendar.getInstance()
    cal.setTimeZone(TimeZone.getTimeZone("JST"))
    val referenceTime = t(0)
    cal.setTimeInMillis(referenceTime)



    val sdf = new SimpleDateFormat("yyyy/MM/dd (E)")
    val dateMessage = sdf.format(cal.getTime)

    val resPerDay = 9
    val duration: Int = 24 / (resPerDay - 1)
    val xTick = new Array[Double](resPerDay)
    val xTickLabel = new Array[String](resPerDay)
    for (i <- 0 until resPerDay) {
      cal.set(Calendar.HOUR_OF_DAY, i * duration)
      cal.set(Calendar.MINUTE, 0)
      cal.set(Calendar.SECOND, 0)
      xTick(i) = cal.getTimeInMillis
      val hour = i * duration
      xTickLabel(i) = s"${hour}:00"
    }

    val sensorInfo = Array(x, y, z)
    val accelAxisLabels = Array("x", "y", "z")

    for (i <- 0 until sensorInfo.length) {
      subplot(sensorInfo.length, 1, i + 1)
      scatter(t, sensorInfo(i), "bo")
      xlabel(s"time ($dateMessage)")
      ylabel(accelAxisLabels(i))
      box(BoxArg.on)
      set(gca, "XTick", xTick)
      set(gca, "XTickLabel", xTickLabel)
    }
  }
}

