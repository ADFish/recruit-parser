/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

/**
  * This case class is the in process for loading the raw csv
  */

/**
  *
  * @param jobType job type
  * @param team Team
  * @param badgeId user id
  * @param beginDate Date for begin count KPI
  * @param endDate Date for begin count KPI
  * @param kpiRA1 KPI1
  * @param kpiRA2 KPI2
  * @param kpiRA3 KPI3
  * @param kpiRA4 KPI4
  * @param kpiRA5 KPI5
  * @param kpiRA6 KPI6
  * @param kpiRA7 KPI7
  * @param kpiRA8 KPI8
  * @param caReferralItem Number of items introduced by CA team
  * @param caReferralHeadcount Number of people introduced by CA team
  * @param informalDecisionHeadcount Number of people decided to accept the offer
  */
case class KPIRARawData(
                         jobType: Int,
                         team: String,
                         badgeId: Int,
                         beginDate: String,
                         endDate: String,
                         kpiRA1: Int,
                         kpiRA2: Int,
                         kpiRA3: Int,
                         kpiRA4: Double,
                         kpiRA5: Double,
                         kpiRA6: Double,
                         kpiRA7: Int,
                         kpiRA8: Int,
                         caReferralItem: Int,
                         caReferralHeadcount: Int,
                         informalDecisionHeadcount: Int
                       )
