/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.{Dataset, SparkSession}

/** we assume each line contains the following type information: define features, badge_id: Int, energy: Double, consistency: Double,
  * x: Double, y: Double, z: Double, timestamp: Long
  * An example for the line is as follow:
  * save features badge_id "2850" energy "-0.00929201" consistency "0.991038" x "-0.186328" y "0.655859" z "-0.71875" timestamp "1445459070786356"
  * Created by duan on 12/11/17.
  */
object AccelParser {
  /**
    * This method load the accelerometer sensor data
    * @param session Spark session
    * @param filePath The accelerometer sensor data path
    * @return AccelData dataset
    */
  def loadDataset(session: SparkSession, filePath: String): Dataset[AccelData] = {
    val lines = session.sparkContext.textFile(filePath)

    val replacedLines = lines.map(_.replaceAll("\"", ""))

    val accelRDD = replacedLines.filter { line =>
      val splits = line.split(" ")
      splits.length == 16 &&
        line.startsWith("save features")
    }.
      map { line =>
        val splits = line.split(" ")
        AccelData(splits(3).toInt,
          splits(5).toDouble,
          splits(7).toDouble,
          splits(9).toDouble,
          splits(11).toDouble,
          splits(13).toDouble,
          splits(15).toLong / 1000L)
      }

    import session.implicits._
    accelRDD.toDS()
  }
}
