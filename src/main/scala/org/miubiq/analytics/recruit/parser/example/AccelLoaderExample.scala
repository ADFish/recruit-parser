/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.analytics.recruit.parser.{AccelParser, AccelSpectrumVisualizer, AccelSpecturmConverter, AccelVisualizer}
import org.miubiq.analytics.recruit.parser.{AccelSpectrumVisualizer, AccelSpecturmConverter, AccelVisualizer}
import org.miubiq.commons.mwplot.Plotter._

/**
  * Created by duan on 12/11/17.
  */
object AccelLoaderExample {
  def main(args: Array[String]): Unit = {

    val session = startUpSpark()

    val path = "testData/recruit/FileExample/2850_001/storage_slow/2850_accel_storage_20151027T000249_26-2.txt"

    val accelDataset = AccelParser.loadDataset(session, path)

    accelDataset.printSchema()
    accelDataset.show()

    figure(1)
    AccelVisualizer.view(accelDataset)

    figure(2)
    // spectrum on 128 frames per 60 sec.
    // we assume sampling rate is 2 [Hz]. i.e. we fetch 64 sec samples
    // to obtain spectrum
    val spectrumInfo = AccelSpecturmConverter.get(accelDataset, 128, 120)

    AccelSpectrumVisualizer.showSpectrum(spectrumInfo.collect())
  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("accelParser")
      .getOrCreate
  }
}
