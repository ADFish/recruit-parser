/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import java.io.File
import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import breeze.linalg.DenseMatrix
import org.miubiq.commons.adapter.BreezeMwInputParser
import org.miubiq.commons.io.MiubiqConf
import org.miubiq.commons.mwplot.Plotter
import org.miubiq.commons.mwplot.Plotter._

/**
  * Created by simosaka on 2018/01/07.
  */
object AccelSpectrumVisualizer {

  /**
    * This method show the spectrum figure of accelerometer sensor data.
    * @param samples The spectrum of 3 axes with timestamp
    * @return Show the spectrum figure of accelerometer sensor data.
    */
  def showSpectrum(samples: Array[AccelSpectrum3AxesWithTime]): Unit = {

    if (samples.isEmpty) {
      val x = Array.empty[Int]
      val y = Array.empty[Int]

      Plotter.setParser(BreezeMwInputParser)
      Plotter.stem(x, y)

    } else {
      val n = samples.length

      val spectrumDim = samples(0).xSpectrum.length

      val xSpectrum = new DenseMatrix[Double](spectrumDim, n)
      val ySpectrum = xSpectrum.copy
      val zSpectrum = xSpectrum.copy

      val timeInfo = new Array[Double](n)

      for (i <- samples.indices) {
        xSpectrum(::, i) := samples(i).xSpectrum
        ySpectrum(::, i) := samples(i).ySpectrum
        zSpectrum(::, i) := samples(i).zSpectrum
        timeInfo(i) = samples(i).unixTimeStamp
      }

      val sensorInfo = Array(xSpectrum, ySpectrum, zSpectrum)

      val accelAxisLabels = Array(
        "x Amplitude [Hz]",
        "y Amplitude [Hz]",
        "z Amplitude [Hz]"
      )

      Plotter.setParser(BreezeMwInputParser)

      val cal = Calendar.getInstance()
      cal.setTimeZone(TimeZone.getTimeZone("JST"))
      val referenceTime = timeInfo(0).toLong
      cal.setTimeInMillis(referenceTime)
      val sdf = new SimpleDateFormat("yyyy/MM/dd (E)")
      val dateMessage = sdf.format(cal.getTime)

      val resPerDay = 9
      val duration: Int = 24 / (resPerDay - 1)
      val xTick = new Array[Double](resPerDay)
      val xTickLabel = new Array[String](resPerDay)
      for (i <- 0 until resPerDay) {
        cal.set(Calendar.HOUR_OF_DAY, i * duration)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        xTick(i) = cal.getTimeInMillis
        val hour = i * duration
        xTickLabel(i) = s"${hour}:00"
      }


      for (i <- accelAxisLabels.indices) {
        subplot(accelAxisLabels.length, 1, i + 1)

        val freq = samples(0).freq
        imagesc(timeInfo, freq, sensorInfo(i))
        set(gca(), "YDir", "normal")
        colorbar()

        ylabel(accelAxisLabels(i))
        xlabel(s"time ($dateMessage)")
        set(gca, "XTick", xTick)
        set(gca, "XTickLabel", xTickLabel)
        xlim(Array(xTick(0), xTick.last))
      }
    }
  }

  /**
    * This method show the spectrum figure of accelerometer sensor data in a given date.
    * @param samples The spectrum of 3 axes with timestamp
    * @param savePath The path to save the visualization result
    * @param date the date of the sensor data generated
    * @return Show the spectrum figure of accelerometer sensor data.
    */
  def showSpectrum(samples: Array[AccelSpectrum3AxesWithTime], savePath: String, date: String): Unit = {

    Plotter.figure(3)
    showSpectrum(samples)
    val saveDir = new File(MiubiqConf.expOutPath(savePath))
    if (!saveDir.exists) {
      saveDir.mkdirs()
    }

    Plotter.saveas(3, saveDir + File.separator + s"${savePath}_accelSpectrum_${date}.fig")
    Plotter.saveas(3, saveDir + File.separator + s"${savePath}_accelSpectrum_${date}.png")
    Plotter.saveas(3, saveDir + File.separator + s"${savePath}_accelSpectrum_${date}.pdf")
    Plotter.saveas(3, saveDir + File.separator + s"${savePath}_accelSpectrum_${date}.eps", "epsc2")
    Plotter.close(3)

  }

}
