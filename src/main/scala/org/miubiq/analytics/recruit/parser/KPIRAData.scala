/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

/** In the raw csv:
  *
  * KPI_RA_1(新規契約_社数),KPI_RA_2(新規承認求人_枚数),KPI_RA_3(CA推薦_件数 - RAOUT_件数),
  * KPI_RA_4((CA推薦_件数 - RAOUT_件数)/CA推薦_件数),KPI_RA_5(CA推薦_人数-RAOUT_人数),
  * KPI_RA_6((CA推薦_人数-RAOUT_人数)/CA推薦_人数),KPI_RA_7(企業内定_件数),KPI_RA_8(決定_人数)
  *
  * In order to be consistent with CA team, here we add 企業内定_人数 (informal_decision_headcount)
  * and changed the KPI order as:
  *
  * KPI_CA_1(CA推薦_件数 - RAOUT_件数),KPI_CA_2(( CA推薦_件数 - RAOUT_件数 ) /CA推薦_件数),KPI_CA_3(CA推薦_人数-RAOUT_人数),
  * KPI_CA_4(( CA推薦_人数-RAOUT_人数 ) /CA推薦_人数),KPI_CA_5(企業内定_件数),KPI_CA_6(企業内定_人数),KPI_CA_7(決定_人数),
  * KPI_RA_8(新規契約_社数),KPI_RA_9(新規承認求人_枚数)
  *
  */

/**
  *
  * @param jobType job type
  * @param team Team
  * @param badgeId user id
  * @param beginDate Date for begin count KPI
  * @param endDate Date for begin count KPI
  * @param kpiRA1 KPI1
  * @param kpiRA2 KPI2
  * @param kpiRA3 KPI3
  * @param kpiRA4 KPI4
  * @param kpiRA5 KPI5
  * @param kpiRA6 KPI6
  * @param kpiRA7 KPI7
  * @param kpiRA8 KPI8
  * @param kpiRA9 KPI9
  */

case class KPIRAData(
                      jobType: Int,
                      team: String,
                      badgeId: Int,
                      beginDate: String,
                      endDate: String,
                      kpiRA1: Double,
                      kpiRA2: Double,
                      kpiRA3: Double,
                      kpiRA4: Double,
                      kpiRA5: Double,
                      kpiRA6: Double,
                      kpiRA7: Double,
                      kpiRA8: Double,
                      kpiRA9: Double
                    )


