/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.Dataset

class KPIGetter {

  /**
    * This method get the given kind of  KPI of a user
    * @param id user's id
    * @param caKPIDataset KPI dataset of CA team
    * @param raKPIDataset KPI dataset of RA team
    * @param kpiKind which kind of KPI to get
    * @return KPI value
    */
  def getKPI(id: Int, caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData], kpiKind: Int): Double = {
    val kpiParser = new KPIParser

    kpiKind match {
      case 1 => userKPI1Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case 2 => userKPI2Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case 3 => userKPI3Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case 4 => userKPI4Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case 5 => userKPI5Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case 6 => userKPI6Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case 7 => userKPI7Map(caKPIDataset, raKPIDataset).getOrElse(id, Double.NaN)
      case _ => throw new IllegalArgumentException("kpi kinds should among 1 ~ 7")
    }

  }

  private def userKPI1Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA1)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA1)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

  private def userKPI2Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA2)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA2)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

  private def userKPI3Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA3)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA3)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

  private def userKPI4Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA4)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA4)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

  private def userKPI5Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA5)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA5)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

  private def userKPI6Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA6)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA6)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

  private def userKPI7Map(caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData]): Map[Int, Double] = {
    val caMap = caKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiCA7)
    }.
      collect.
      toMap

    val raMap = raKPIDataset.rdd.map { row =>
      (row.badgeId, row.kpiRA7)
    }.
      collect.
      toMap

    caMap ++ raMap
  }

}
