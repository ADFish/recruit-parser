/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

/**
  * This case class is the in process for loading the raw csv
  */

/**
  *
  * @param jobType job type
  * @param team Team
  * @param badgeId user id
  * @param beginDate Date for begin count KPI
  * @param endDate Date for begin count KPI
  * @param kpiCA1 KPI1
  * @param kpiCA2 KPI2
  * @param kpiCA3 KPI3
  * @param kpiCA4 KPI4
  * @param kpiCA5 KPI5
  * @param kpiCA6 KPI6
  * @param kpiCA7 KPI7
  * @param caReferralItem Number of items introduced by CA team
  * @param caReferralHeadcount Number of people introduced by CA team
  */
case class KPICARawData(
                         jobType: Int,
                         team: String,
                         badgeId: Int,
                         beginDate: String,
                         endDate: String,
                         kpiCA1: Int,
                         kpiCA2: Double,
                         kpiCA3: Int,
                         kpiCA4: Double,
                         kpiCA5: Double,
                         kpiCA6: Double,
                         kpiCA7: Int,
                         caReferralItem: Int,
                         caReferralHeadcount: Int
                       )
