/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.{Dataset, SparkSession}


/** we assume each line contains the following type information:
  * save detectionts, badge_id : Int, other_id : Int, other_timestamp : Long, timestamp : Long
  * An example for the line is as follow:
  * save detectionts badge_id "2850" other_id "2970" other_timestamp "000522a32ab5efd3" timestamp "1445459127033826"
  * Created by duan on 12/11/17.
  */
object InfraParser {
  /**
    * This method laod the infrared sensor dataset
    * @param session Spark session
    * @param filePath File path for saving the infrared sensor data
    * @return A dataset of InfraData
    */
  def loadDataset(session: SparkSession, filePath: String): Dataset[InfraData] = {

    val lines = session.sparkContext.textFile(filePath)
    val infraRDD = lines.filter {
      line => line.startsWith("save detectionts")
    }.map { line =>
      val replacedLine = line.replaceAll("\"", "")
      val splits = replacedLine.split(" ")
      InfraData(splits(3).toInt,
        splits(5).toInt,
        splits(9).toLong / 1000L)
    }
    import session.implicits._
    infraRDD.toDS
  }
}
