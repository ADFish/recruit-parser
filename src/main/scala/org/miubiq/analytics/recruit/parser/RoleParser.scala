/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.{Dataset, SparkSession}


/** we assume each line contains the following type information:
  * Personal ID,Participation,Badge ID,Data Directory,Team,Job Type,Job Role,KPI Data
  * A typical example is as follow:
  * 1,Participation,2850,2850_001,RA_2G,1,Sales Staff ,w
  * Created by duan on 1/18/18.
  */

class RoleParser {

  /**
    * This method load the role information
    * @param session Spark session
    * @param filePath File path for saving the role data.
    * @return RoleData dataset
    */
  def loadDataset(session: SparkSession, filePath: String): Dataset[RoleData] = {

    val roleDF = session.read
      .option("header", "true")
      .option("inferSchema", value = true)
      .csv(filePath)

    import session.implicits._
    roleDF.map { row =>
      val personalId = row.getAs[Int]("Personal ID")
      val participation = row.getAs[String]("Participation")
      val badgeId = row.getAs[Int]("Badge ID")
      val dataDirectory = row.getAs[String]("Data Directory")
      val team = row.getAs[String]("Team")
      val jobType = row.getAs[Int]("Job Type")
      val jobRole = row.getAs[String]("Job Role")
      val KPIData = row.getAs[String]("KPI Data")

      RoleData(personalId, participation, badgeId, dataDirectory, team, jobType, jobRole, KPIData)
    }.as[RoleData]
  }

}
