/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.analytics.recruit.parser.InfraParser

/**
  * Created by duan on 12/11/17.
  */
object InfraLoaderExample {
  def main(args: Array[String]): Unit = {
    val session = startUpSpark()
    val path = "testData/recruit/FileExample/2850_001/storage_slow/2850_infrared_storage_20151027T000249_26-1.txt"
    val infraDataset = InfraParser.loadDataset(session, path)
    infraDataset.printSchema()
    infraDataset.show()
  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("infraParser")
      .getOrCreate
  }
}
