/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.{Dataset, SparkSession}


/** we assume each line contains the following type information:
  * save detextion, badge_id : Int, unixTimeStamp : Long, other_bt : String, bt_class : Sting, rssi : Int
  * An example for the line is as follow:
  * save detection badge_id "2850" timestamp "1445578599309423" other_bt "00:07:80:0e:8d:f1" bt_class "001f00" rssi "-77"
  * Created by duan on 12/18/17.
  */
object BluetoothParser {
  /**
    * This method load bluetooth dataset
    * @param session Spark session
    * @param filePath File path to save the bluetooth sensor data.
    * @return A dataset of BluetoothData
    */
  def loadDataset(session: SparkSession, filePath: String): Dataset[BluetoothData] = {

    val lines = session.sparkContext.textFile(filePath)

    val replacedLines = lines.map(_.replaceAll("\"", ""))

    val bluetoothRDD = replacedLines.filter { line =>
      val splits = line.split(" ")
      line.startsWith("save detection") &&
        isStr2Num(splits(11))
    }.
      map { line =>
        val splits = line.split(" ")
        if (splits(7) == "5c:f3:706?a:44:3f") {
          splits(7) = "5c:f3:70:6a:44:3f"
        }
        BluetoothData(splits(3).toInt,
          splits(5).toLong / 1000L,
          splits(7),
          splits(9),
          splits(11).toInt)
      }
    import session.implicits._
    bluetoothRDD.toDS()
  }

  private def isStr2Num(str: String): Boolean = {
    try {
      Integer.parseInt(str)
      return true
    } catch {
      case ex: NumberFormatException => return false
    }
  }

}
