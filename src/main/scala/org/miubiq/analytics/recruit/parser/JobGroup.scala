/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.Dataset

/**
  * Created by duan on 1/27/18.
  */
class JobGroup(val roleDataset: Dataset[RoleData]) extends Serializable {

  val RA_2G = "RA_2G"
  val RA_3G = "RA_3G"

  val CA_3G = "CA_3G"
  val CA_4G = Array("CA_4G", "CA_4G_H", "CA_4G_I", "CA_4G_K", "CA_4G_O")

  val RA = Array(RA_2G, RA_3G)
  val CA = CA_4G :+ CA_3G

  val other = "other"

  private val teamMember = groupUsersByTeam

  private val badgesPerGroup = teamMember.map {
    case (teamName, players) => {
      val badgeIds = players.map(_.badgeId)
      (teamName, badgeIds)
    }
  }

  val teamRA2 = badgesPerGroup(RA_2G)
  val teamRA3 = badgesPerGroup(RA_3G)
  val teamRA = teamRA2 ++ teamRA3

  val teamCA3 = badgesPerGroup(CA_3G)
  val teamCA4 = CA_4G.flatMap(badgesPerGroup(_))
  val teamCA = teamCA3 ++ teamCA4

  val otherTeam = badgesPerGroup(other)

  private def groupUsersByTeam: Map[String, Set[RoleData]] = {
    roleDataset.rdd.groupBy(_.team)
      .map {
        case (teamName, players) => {
          (teamName, players.toSet)
        }
      }.collect
      .toMap
  }

  /**
    * Get the team name of a user
    * @param id User's id
    * @return Team name
    */
  def getTeam(id: Int): String = {
    if (teamRA2.contains(id)) {
      RA_2G
    } else if (teamRA3.contains(id)) {
      RA_3G
    } else if (teamCA3.contains(id)) {
      CA_3G
    } else if (teamCA4.contains(id)) {
      "CA_4G"
    } else if (otherTeam.contains(id)) {
      "other"
    } else {
      "no-data"
    }
  }

}
