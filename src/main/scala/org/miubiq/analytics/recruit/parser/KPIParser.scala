/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.{Dataset, SparkSession}

/**
  * Created by duan on 3/10/18.
  */

class KPIParser extends Serializable {

  /**
    * We assume each line contains the following type information:
    *
    * 0職位ID,1部署ID,2デバイスID,3期間_FROM,4期間_TO,5受付_人数,
    * 6初回面談_人数,7登録_人数,8資料送付_件数,9資料送付_人数,10CA推薦_件数,
    * 11CA推薦_人数,12RAOUT_件数,13RAOUT_人数,14書類OUT_件数,15書類OUT_人数,
    * 16書類OK_件数,17企業推薦-書類OK件数率,18書類OK_人数,19一面実施_件数,20一面実施_人数,
    * 21一面OUT_件数,22一面OUT_人数,23一面OK_件数,24一面OK_人数,25企業内定_件数,26企業内定_人数,27決定_人数,
    * 28KPI_CA_1(CA推薦_件数 - RAOUT_件数),29KPI_CA_2(( CA推薦_件数 - RAOUT_件数 ) /CA推薦_件数),
    * 30KPI_CA_3(CA推薦_人数-RAOUT_人数),31KPI_CA_4(( CA推薦_人数-RAOUT_人数 ) /CA推薦_人数),
    * 32KPI_CA_5(企業内定_件数),33KPI_CA_6(企業内定_人数),34KPI_CA_7(決定_人数)
    *
    * An example for the line is as follow:
    *
    * 1,CA_3G,2876,2015/9/28,2015/10/4,7,5,4,1075 ,91,84,25,49,17,16,14,14,48.3%,11,7,5,1,1,6,5,4,4,10,35,0.417 ,8,0.320 ,4 ,4 ,10
    *
    * And we choose following KPI for loading:
    *
    * KPI_CA_1(CA推薦_件数 - RAOUT_件数),KPI_CA_2(( CA推薦_件数 - RAOUT_件数 ) /CA推薦_件数),KPI_CA_3(CA推薦_人数-RAOUT_人数),
    * KPI_CA_4(( CA推薦_人数-RAOUT_人数 ) /CA推薦_人数),KPI_CA_5(企業内定_件数),KPI_CA_6(企業内定_人数),KPI_CA_7(決定_人数)
    */

  /**
    * Load the raw KPI data for CA team
    * @param session Spark session
    * @param filePath The file path for saving the KPI data
    * @return A dataset of KPICARawData
    */
  def loadCARawDataset(session: SparkSession, filePath: String): Dataset[KPICARawData] = {

    val kpiCADF = session.read.
      option("header", "true").
      option("inferSchema", value = true).
      csv(filePath)

    import session.implicits._
    val rawDataset = kpiCADF.filter { row =>
      row(0).toString != "#N/A" &&
        !row(1).toString.contains("RA")
    }.
      map { row =>
        val jobType = row(0).toString.toInt
        val team = row(1).toString
        val badgeId = row(2).toString.toInt
        val beginDate = row(3).toString
        val endDate = row(4).toString
        val kpi1 = row(28).toString.toInt
        val kpi2 = row(29).toString.toDouble
        val kpi3 = row(30).toString.toInt
        val kpi4 = row(31).toString.toDouble
        val kpi5 = row(32).toString.toDouble
        val kpi6 = row(33).toString.toDouble
        val kpi7 = row(34).toString.toInt
        val caReferralItem = row(10).toString.toInt
        val caReferralHeadcount = row(11).toString.toInt
        KPICARawData(jobType, team, badgeId, beginDate, endDate,
          kpi1, kpi2, kpi3, kpi4, kpi5, kpi6, kpi7,
          caReferralItem, caReferralHeadcount)
      }.as[KPICARawData]

    val user2985Data = count2958Data(session)

    rawDataset.union(user2985Data)
  }

  /**
    * Load CA Team's KPI between 20150928 ~ 20151206
    *
    * @param session Spark session
    * @param filePath The file path for saving the raw KPI data
    * @return A dataset of KPICAData
    */
  def loadCADataset(session: SparkSession, filePath: String): Dataset[KPICAData] = {

    val rawDataset = loadCARawDataset(session, filePath)

    val kpiRDD = rawDataset.rdd.groupBy(_.badgeId).map { row =>
      val jobType = row._2.head.jobType
      val team = row._2.head.team
      val badgeId = row._1
      val filteredDate = row._2.filter { row =>
        val beginDate = reformatDate(row.beginDate).toInt
        val endDate = reformatDate(row.endDate).toInt
        beginDate >= 20150928 &&
          endDate <= 20151206
      }

      val beginDate = "20150928"
      val endDate = "20151206"

      val totalCaReferralItems = filteredDate.map {
        _.caReferralItem
      }.sum

      val totalCaReferralHeadcounts = filteredDate.map {
        _.caReferralHeadcount
      }.sum

      val kpi1 = filteredDate.map(_.kpiCA1).sum
      val kpi2 = totalCaReferralItems match {
        case 0 => 0.0
        case _ => kpi1 / totalCaReferralItems.toDouble
      }
      val kpi3 = filteredDate.map(_.kpiCA3).sum
      val kpi4 = totalCaReferralHeadcounts match {
        case 0 => 0.0
        case _ => kpi3 / totalCaReferralHeadcounts.toDouble
      }
      val kpi5 = filteredDate.map(_.kpiCA5).sum
      val kpi6 = filteredDate.map(_.kpiCA6).sum
      val kpi7 = filteredDate.map(_.kpiCA7).sum

      KPICAData(jobType, team, badgeId, beginDate, endDate, kpi1, kpi2, kpi3, kpi4, kpi5, kpi6, kpi7)
    }

    import session.implicits._
    session.createDataset[KPICAData](kpiRDD)
  }


  /** We assume each line contains the following type information:
    *
    * 0職位ID,1部署ID,2デバイスID,3期間_FROM,4期間_TO,5新規契約_社数,
    * 6新規活動_社数,7新規承認求人_社数,8新規承認求人_枚数,9復活求人_社数,10資料送付_枚数,
    * 11資料送付_件数,12CA推薦_枚数,13CA推薦_件数,14RA-OUT_枚数,15RA-OUT_件数,
    * 16企業推薦_枚数,17企業推薦_件数,18書類OUT_枚数,19書類OUT_件数,20書類OK_枚数,
    * 21書類OK_件数,22一面実施_枚数,23一面実施_件数,24一面OUT_枚数,25一面OUT_件数,
    * 26一面OK_枚数,27一面OK_件数,28企業内定_社数,29企業内定_枚数,30企業内定_件数,31決定_社数,32決定_枚数,33決定_人数,
    * 34KPI_RA_1(新規契約_社数),35KPI_RA_2(新規承認求人_枚数),
    * 36KPI_RA_3(CA推薦_件数 - RAOUT_件数),37KPI_RA_4((CA推薦_件数 - RAOUT_件数)/CA推薦_件数),
    * 38KPI_RA_5(CA推薦_人数-RAOUT_人数),39KPI_RA_6((CA推薦_人数-RAOUT_人数)/CA推薦_人数),
    * 40KPI_RA_7(企業内定_件数),41KPI_RA_8(決定_人数)
    *
    * In order to be consistent with CA team, here we add 企業内定_枚数 as 企業内定_人数 (informal_decision_headcount)
    * and changed the order as:
    *
    * KPI_CA_1(CA推薦_件数 - RAOUT_件数),KPI_CA_2(( CA推薦_件数 - RAOUT_件数 ) /CA推薦_件数),KPI_CA_3(CA推薦_人数-RAOUT_人数),
    * KPI_CA_4(( CA推薦_人数-RAOUT_人数 ) /CA推薦_人数),KPI_CA_5(企業内定_件数),KPI_CA_6(企業内定_人数),KPI_CA_7(決定_人数),
    * KPI_RA_8(新規契約_社数),KPI_RA_9(新規承認求人_枚数)
    *
    */

  /**
    * Load the raw KPI data for RA team
    * @param session Spark session
    * @param filePath The file path for saving the KPI data
    * @return A dataset of KPIRARawData
    */
  def loadRARawDataset(session: SparkSession, filePath: String): Dataset[KPIRARawData] = {

    val kpiRADF = session.read.
      option("header", "true").
      option("inferSchema", value = true).
      csv(filePath)

    import session.implicits._
    kpiRADF.map {
      row =>
        val jobType = row(0).toString.toInt
        val team = row(1).toString
        val badgeId = row(2).toString.toInt
        val beginDate = row(3).toString
        val endDate = row(4).toString
        val kpi1 = row(34).toString.toInt
        val kpi2 = row(35).toString.toInt
        val kpi3 = row(36).toString.toInt
        val kpi4 = row(37).toString.toDouble
        val kpi5 = row(38).toString.toDouble
        val kpi6 = row(39).toString.toDouble
        val kpi7 = row(40).toString.toInt
        val kpi8 = row(41).toString.toInt
        val caReferralItem = row(13).toString.toInt
        val caReferralHeadcount = row(12).toString.toInt
        val informal_decision_headcount = row(29).toString.toInt

        KPIRARawData(jobType, team, badgeId, beginDate, endDate,
          kpi1, kpi2, kpi3, kpi4, kpi5, kpi6, kpi7, kpi8,
          caReferralItem, caReferralHeadcount, informal_decision_headcount)
    }.as[KPIRARawData]

  }

  /**
    * Load CA Team's KPI between 20150928 ~ 20151206
    *
    * @param session Spark session
    * @param filePath The file path for saving the raw KPI data
    * @return A dataset of KPICAData
    */
  def loadRADataset(session: SparkSession, filePath: String): Dataset[KPIRAData] = {

    val rawDataset = loadRARawDataset(session, filePath)
    val kpiRDD = rawDataset.rdd.groupBy(_.badgeId).map { row =>
      val jobType = row._2.head.jobType
      val team = row._2.head.team
      val badgeId = row._1
      val filteredDate = row._2.filter { row =>
        val beginDate = reformatDate(row.beginDate).toInt
        val endDate = reformatDate(row.endDate).toInt
        beginDate >= 20150928 &&
          endDate <= 20151206
      }

      val beginDate = "20150928"
      val endDate = "20151206"

      val totalCaReferralItems = filteredDate.map {
        _.caReferralItem
      }.sum

      val totalCaReferralHeadcounts = filteredDate.map {
        _.caReferralHeadcount
      }.sum

      val kpi1 = filteredDate.map(_.kpiRA3).sum
      val kpi2 = totalCaReferralItems match {
        case 0 => 0.0
        case _ => kpi1 / totalCaReferralItems.toDouble
      }
      val kpi3 = filteredDate.map(_.kpiRA5).sum
      val kpi4 = totalCaReferralHeadcounts match {
        case 0 => 0.0
        case _ => kpi3 / totalCaReferralHeadcounts.toDouble
      }
      val kpi5 = filteredDate.map(_.kpiRA7).sum
      val kpi6 = filteredDate.map(_.informalDecisionHeadcount).sum
      val kpi7 = filteredDate.map(_.kpiRA8).sum
      val kpi8 = filteredDate.map(_.kpiRA1).sum
      val kpi9 = filteredDate.map(_.kpiRA2).sum

      KPIRAData(jobType, team, badgeId, beginDate, endDate,
        kpi1, kpi2, kpi3, kpi4, kpi5, kpi6, kpi7, kpi8, kpi9)
    }

    import session.implicits._
    session.createDataset[KPIRAData](kpiRDD)
  }

  private def reformatDate(date: String): String = {
    val splits = date.split("/")
    if (splits(1).length == 1) {
      splits(1) = "0" + splits(1)
    }
    if (splits(2).length == 1) {
      splits(2) = "0" + splits(2)
    }
    splits.mkString("")
  }

  /**
    *
    * User 2958's data is missing in the KPIdata_fix_CA.csv file, but some data of
    * 2958 is founded in KPIdata_update20160117.xlsx
    * But the data itself have contradicts. Here We manually add some 2985's data
    *
    * @see testData/recruit/KPIdata_update20160117.xlsx
    */
  private def count2958Data(session: SparkSession): Dataset[KPICARawData] = {

    val jobType = 1
    val team = "CA_3G"
    val userId = 2958

    /*
    Personal_ID	Badge_ID	Participation	Team	dur_FROM	dur_TO	CA推薦_件数	 CA推薦_人数	RAOUT_件数	RAOUT_人数	企業内定_件数	企業内定_人数	決定_人数
        59	2958	Participation	CA_3G	2015/10/19	2015/10/25	0 	0 	0 	0 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/02	2015/11/08	0 	0 	0 	0 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/09	2015/11/15	0 	0 	0 	0 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/30	2015/12/06	0 	0 	0 	0 	0 	0 	0
        59	2958	Participation	CA_3G	2015/09/28	2015/10/04	19 	12 	7 	4 	2 	2 	7
        59	2958	Participation	CA_3G	2015/10/05	2015/10/11	33 	13 	17 	8 	3 	2 	0
        59	2958	Participation	CA_3G	2015/10/12	2015/10/18	79 	12 	18 	6 	2 	2 	0
        59	2958	Participation	CA_3G	2015/10/19	2015/10/25	67 	17 	29 	13 	2 	2 	0
        59	2958	Participation	CA_3G	2015/10/26	2015/11/01	105 	16 	30 	11 	4 	4 	8
        59	2958	Participation	CA_3G	2015/11/02	2015/11/08	104 	15 	56 	9 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/09	2015/11/15	53 	14 	27 	11 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/16	2015/11/22	92 	23 	25 	17 	2 	2 	0
        59	2958	Participation	CA_3G	2015/11/23	2015/11/29	88 	22 	24 	13 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/30	2015/12/06	70 	22 	26 	15 	1 	1 	1
        59	2958	Participation	CA_3G	2015/10/19	2015/10/25	0 	0 	0 	0 	0 	0 	0
        59	2958	Participation	CA_3G	2015/11/16	2015/11/22	0 	0 	0 	0 	0 	0 	0
        */

    val rawData = new Array[KPICARawData](10)
    rawData(0) = KPICARawData(jobType, team, userId, "2015/9/28", "2015/10/4", 19 - 7, (19 - 7) / 19.0,
      12 - 4, (12 - 4) / 12.0, 2, 2, 7, 19, 12)

    rawData(1) = KPICARawData(jobType, team, userId, "2015/10/5", "2015/10/11", 33 - 17, (33 - 17) / 33.0,
      13 - 8, (13 - 8) / 13.0, 3, 2, 0, 33, 17)

    rawData(2) = KPICARawData(jobType, team, userId, "2015/10/12", "2015/10/18", 79 - 18, (79 - 18) / 79.0,
      12 - 6, (12 - 6) / 12.0, 2, 2, 0, 79, 12)

    rawData(3) = KPICARawData(jobType, team, userId, "2015/10/19", "2015/10/25", 67 - 29, (67 - 29) / 67.0,
      17 - 13, (17 - 13) / 17.0, 2, 2, 0, 67, 17)

    rawData(4) = KPICARawData(jobType, team, userId, "2015/10/26", "2015/11/1", 105 - 30, (105 - 30) / 105.0,
      16 - 11, (16 - 11) / 16.0, 4, 4, 8, 105, 16)

    rawData(5) = KPICARawData(jobType, team, userId, "2015/11/2", "2015/11/8", 104 - 56, (104 - 56) / 104.0,
      15 - 9, (15 - 9) / 15.0, 0, 0, 0, 104, 15)

    rawData(6) = KPICARawData(jobType, team, userId, "2015/11/9", "2015/11/15", 53 - 27, (53 - 27) / 53.0,
      14 - 11, (14 - 11) / 14.0, 0, 0, 0, 53, 14)

    rawData(7) = KPICARawData(jobType, team, userId, "2015/11/16", "2015/11/22", 92 - 25, (92 - 25) / 92.0,
      23 - 17, (23 - 17) / 23.0, 2, 2, 0, 92, 23)

    rawData(8) = KPICARawData(jobType, team, userId, "2015/11/23", "2015/11/29", 88 - 24, (88 - 24) / 88.0,
      22 - 13, (22 - 13) / 22.0, 0, 0, 0, 88, 22)

    rawData(9) = KPICARawData(jobType, team, userId, "2015/11/30", "2015/12/6", 70 - 26, (70 - 26) / 70.0,
      22 - 15, (22 - 15) / 22.0, 1, 1, 1, 70, 22)

    val rdd = session.sparkContext.parallelize(rawData)
    import session.implicits._
    session.createDataset[KPICARawData](rdd)
  }

}



