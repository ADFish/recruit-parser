/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import java.io.File

import org.apache.spark.sql.Dataset

class FileGetter(val roleDataset: Dataset[RoleData], dataPath: String) {

  // The experiment begin date for recruit data is 2015/10/26, and the end date is 2015/11/20
  val beginDate = 20151026
  val endDate = 20151120
  private val userDirectoryMap: Map[Int, String] = roleDataset.rdd.map { row =>
      (row.badgeId, row.dataDirectory)
    }.collect
    .toMap

  /**
    * This method get the directory name for saving the user's raw data
    * @param id User id
    * @return directory name for saving the user's raw data
    */
  def getDirectoryName(id: Int): String = {
    userDirectoryMap(id)
  }

  /**
    * This method get the absolute path where saves the user's raw data
    * @param id User id
    * @return the absolute path where saves the user's raw data
    */
  def getRootPath(id: Int): String = {
    val fileName = getDirectoryName(id)
    dataPath + File.separator + fileName + File.separator + "storage_slow"
  }

  /**
    * We assume the experiment begin date is 2015/10/26, and end date is 2015/11/20
    * The method get all the infrared files' absolute path of one user between the experiment dates
    *
    * @param id
    * @return Array of infrared files' path string
    */
  def getInfraredFiles(id: Int): Set[String] = {
    val rootPath = getRootPath(id)
    val fileList = new File(rootPath).list()
    val infraredFiles = fileList.filter(_.contains("infrared"))
    val dates = infraredFiles.map(getDateNum(_)).
      filter { date =>
        date >= beginDate && date <= endDate
      }

    dates.map { date =>
      getInfraredFile(id, date.toString)
    }.toSet
  }

  /**
    * The method get infrared file's absolute path of one user one day
    *
    * @param id
    * @param date The format of input date should like "20151026"
    * @return Absolute path of one day infrared file
    */
  def getInfraredFile(id: Int, date: String): String = {
    val rootPath = getRootPath(id)
    rootPath + File.separator + s"${id}_infrared_storage_${date}*"
  }

  /**
    * We assume the experiment begin date is 2015/10/26, and end date is 2015/11/20
    * The method get all the accelerometer files' absolute path of one user between the experiment dates
    *
    * @param id
    * @return Array of accelerometer files' path string
    */
  def getAccelFiles(id: Int): Set[String] = {
    val rootPath = getRootPath(id)
    val fileList = new File(rootPath).list()
    val accelFiles = fileList.filter(_.contains("accel"))
    val dates = accelFiles.map(getDateNum(_)).
      filter { date =>
        date >= beginDate && date <= endDate
      }

    dates.map { date =>
      getAccelFile(id, date.toString)
    }.toSet
  }

  /**
    * The method get accelerometer file's absolute path of one user one day
    *
    * @param id
    * @param date The format of input date should like "20151026"
    * @return Absolute path of one day accelerometer file
    */
  def getAccelFile(id: Int, date: String): String = {
    val rootPath = getRootPath(id)
    rootPath + File.separator + s"${id}_accel_storage_${date}*"
  }

  /**
    * We assume the experiment begin date is 2015/10/26, and end date is 2015/11/20
    * The method get all the bluetooth files' absolute path of one user between the experiment dates
    *
    * @param id
    * @return Array of bluetooth files' path string
    */
  def getBluetoothFiles(id: Int): Set[String] = {
    val rootPath = getRootPath(id)
    val fileList = new File(rootPath).list()
    val bluetoothFiles = fileList.filter(_.contains("bluetooth"))
    val dates = bluetoothFiles.map(getDateNum(_)).
      filter { date =>
        date >= beginDate && date <= endDate
      }

    dates.map { date =>
      getBluetoothFile(id, date.toString)
    }.toSet
  }

  /**
    * The method get bluetooth file's absolute path of one user one day
    *
    * @param id
    * @param date The format of input date should like "20151026"
    * @return Absolute path of one day bluetooth file
    */
  def getBluetoothFile(id: Int, date: String): String = {
    val rootPath = getRootPath(id)
    rootPath + File.separator + s"${id}_bluetooth_storage_${date}*"
  }

  /**
    * This method get date from Infrared/Accelerometer/Bluetooth absolute path,
    * this method should be called after getting absolute path
    *
    * @param pathString Infrared/Accelerometer/Bluetooth absolute path
    * @return Date Format as "20151026"
    */
  def getDate(pathString: String): String = {
    val splits = pathString.split("/")
    val date = splits(splits.length - 1).
      split("_")(3).
      replace("*", "")
    date
  }

  // Get date number from the file's name, e.g 2850_accel_storage_20151027T000249_26-2.txt -> 20151027
  private def getDateNum(pathString: String): Int = {
    val date = pathString.
      split("_")(3).
      split("T")(0)
    date.toInt
  }

}
