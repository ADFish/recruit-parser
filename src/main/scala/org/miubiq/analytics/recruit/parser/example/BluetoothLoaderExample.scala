/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.analytics.recruit.parser.{BluetoothFingerprintBuilder, BluetoothParser, BluetoothSamplingTimeVisualizer}
import org.miubiq.analytics.recruit.parser.{BluetoothParser, BluetoothSamplingTimeVisualizer}

/**
  * Created by duan on 12/18/17.
  */
object BluetoothLoaderExample {
  def main(args: Array[String]): Unit = {
    val session = startUpSpark()
    val path = "testData/recruit/FileExample/2850_001/storage_slow/2850_bluetooth_storage_20151027T000252_26-8.txt"
    val bluetoothDataset = BluetoothParser.loadDataset(session, path)
    bluetoothDataset.printSchema()
    bluetoothDataset.show

    BluetoothSamplingTimeVisualizer.view(bluetoothDataset.limit(50))

    val fgGenerator = new BluetoothFingerprintBuilder
    val fingerPrintWithTimeInfo = fgGenerator.getFingerprint(bluetoothDataset)


  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("bluetoothParser")
      .getOrCreate
  }
}
