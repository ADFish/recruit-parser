/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import java.io.File

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.commons.adapter.BreezeMwInputParser
import org.miubiq.commons.io.MiubiqConf
import org.miubiq.commons.mwplot.Plotter

/**
  * Created by duan on 4/2/18.
  * This object is not used
  */
object KPIVisualizer {
  def main(args: Array[String]): Unit = {

    val session = startUpSpark()

    val caFilePath = "testData/recruit/KPIdata_fix_CA.csv"
    val raFilePath = "testData/recruit/KPIdata_fix_RA.csv"
    val path = "testData/recruit/MetaData_Osaka_exported.csv"
    val dataPath = MiubiqConf.inPath("recruit2016/RawDataFromRecruit_ver201701/RawDataFromRecruit/Raw")

    val roleParse = new RoleParser
    val roleDataset = roleParse.loadDataset(session, path)

    val metaData = new ParticipantsPropertyCollector(roleDataset, dataPath)

    val kpiParser = new KPIParser

    val caDataset = kpiParser.loadCADataset(session, caFilePath)
    val raDataset = kpiParser.loadRADataset(session, raFilePath)

    val teamCA3 = metaData.jobGroup.teamCA3
    val teamCA4 = metaData.jobGroup.teamCA4
    val teamRA2 = metaData.jobGroup.teamRA2
    val teamRA3 = metaData.jobGroup.teamRA3

    // KPIType
    val i: Int = 5

    val saveDir = new File(MiubiqConf.outPath(s"KPI_Visualize/KPI_$i"))
    if (!saveDir.exists) {
      saveDir.mkdirs()
    }
    val teamCA3W = teamCA3.filter { id =>
      metaData.hasKPIData(id)
    }.toArray
    val teamCA3KPI = teamCA3W.map { id =>
      metaData.getKPI(id, caDataset, raDataset, i)
    }

    Plotter.setParser(BreezeMwInputParser)
    Plotter.figure(1)
    Plotter.scatter(teamCA3W, teamCA3KPI)
    Plotter.xlabel("User ID")
    Plotter.ylabel("KPI")
    Plotter.saveas(1, saveDir + File.separator + s"CA3_kpi$i.fig")
    Plotter.saveas(1, saveDir + File.separator + s"CA3_kpi$i.png")
    Plotter.saveas(1, saveDir + File.separator + s"CA3_kpi$i.pdf")
    Plotter.saveas(1, saveDir + File.separator + s"CA3_kpi$i.eps", "epsc2")
    Plotter.hold(Plotter.HoldArg.off)
    Plotter.close(1)

    val teamCA4W = teamCA4.filter { id =>
      metaData.hasKPIData(id)
    }
    val teamCA4KPI = teamCA4W.map { id =>
      metaData.getKPI(id, caDataset, raDataset, i)
    }

    Plotter.setParser(BreezeMwInputParser)
    Plotter.figure(2)
    Plotter.scatter(teamCA4W, teamCA4KPI)
    Plotter.xlabel("User ID")
    Plotter.ylabel("KPI")
    Plotter.saveas(2, saveDir + File.separator + s"CA4_kpi$i.fig")
    Plotter.saveas(2, saveDir + File.separator + s"CA4_kpi$i.png")
    Plotter.saveas(2, saveDir + File.separator + s"CA4_kpi$i.pdf")
    Plotter.saveas(2, saveDir + File.separator + s"CA4_kpi$i.eps", "epsc2")
    Plotter.hold(Plotter.HoldArg.off)
    Plotter.close(2)

    val teamRA2W = teamRA2.filter { id =>
      metaData.hasKPIData(id)
    }.toArray
    val teamRA2KPI = teamRA2W.map { id =>
      metaData.getKPI(id, caDataset, raDataset, i)
    }

    Plotter.setParser(BreezeMwInputParser)
    Plotter.figure(3)
    Plotter.scatter(teamRA2W, teamRA2KPI)
    Plotter.xlabel("User ID")
    Plotter.ylabel("KPI")
    Plotter.saveas(3, saveDir + File.separator + s"RA2_kpi$i.fig")
    Plotter.saveas(3, saveDir + File.separator + s"RA2_kpi$i.png")
    Plotter.saveas(3, saveDir + File.separator + s"RA2_kpi$i.pdf")
    Plotter.saveas(3, saveDir + File.separator + s"RA2_kpi$i.eps", "epsc2")
    Plotter.hold(Plotter.HoldArg.off)
    Plotter.close(3)

    val teamRA3W = teamRA3.filter { id =>
      metaData.hasKPIData(id)
    }.toArray
    val teamRA3KPI = teamRA3W.map { id =>
      metaData.getKPI(id, caDataset, raDataset, i)
    }

    Plotter.setParser(BreezeMwInputParser)
    Plotter.figure(4)
    Plotter.scatter(teamRA3W, teamRA3KPI)
    Plotter.xlabel("User ID")
    Plotter.ylabel("KPI")
    Plotter.saveas(4, saveDir + File.separator + s"RA3_kpi$i.fig")
    Plotter.saveas(4, saveDir + File.separator + s"RA3_kpi$i.png")
    Plotter.saveas(4, saveDir + File.separator + s"RA3_kpi$i.pdf")
    Plotter.saveas(4, saveDir + File.separator + s"RA3_kpi$i.eps", "epsc2")
    Plotter.hold(Plotter.HoldArg.off)
    Plotter.close(4)
  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("kpiParserCA")
      .getOrCreate
  }

}
