/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import breeze.linalg.DenseMatrix
import org.apache.commons.csv.{CSVFormat, CSVPrinter}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.commons.exp.config.parser.ArgConfParser
import org.miubiq.commons.io.MiubiqConf

/**
  * Created by duan on 2/6/18.
  * This object Collect the total face to face communication times between each pair of users, which is used for visualizing the F2F social network
  */
object InfraEventAdjacentMatrix extends Serializable {

  def main(args: Array[String]): Unit = {

    val parser = new ArgConfParser(args(0), getClass.getPackage.getName)

    val session = startUpSpark()

    val roleParser = new RoleParser
    val rolePath = "testData/recruit/MetaData_Osaka_exported.csv"
    val roleDataset = roleParser.loadDataset(session, rolePath)

    val dataPath = MiubiqConf.inPath("recruit2016/RawDataFromRecruit_ver201701/RawDataFromRecruit/Raw")
    val metaData = new ParticipantsPropertyCollector(roleDataset, dataPath)

    val kpiParser = new KPIParser
    val caFilePath = "testData/recruit/KPIdata_fix_CA.csv"
    val raFilePath = "testData/recruit/KPIdata_fix_RA.csv"
    val caDataset = kpiParser.loadCADataset(session, caFilePath)
    val raDataset = kpiParser.loadRADataset(session, raFilePath)

    val users = metaData.participantsIds
    val alias = users.map { id =>
      metaData.getParticipantsAlias(id)
    }

    val n = users.length

    val adjacentMatrix = DenseMatrix.zeros[Double](n, n)

    for (i <- users.indices) {
      val infraredFiles = metaData.fileGetter.getInfraredFiles(users(i))

      if (!infraredFiles.isEmpty) {

        val infraredDataset = InfraParser.loadDataset(session, infraredFiles.mkString(","))
        val eventInfoRDD = InfraEventCollector.gather(infraredDataset).sortBy(_.otherID, false).collect()

        val opponentsTimes = eventInfoRDD.filter { row =>
          metaData.isParticipants(row.otherID)
        }.
          map { row =>
            (row.otherID, row.timeArray.length)
          }

        val opponents = opponentsTimes.map(_._1)

        val opponentsAlias = opponents.map { id =>
          metaData.getParticipantsAlias(id)
        }

        val times = opponentsTimes.map(_._2)

        for (j <- opponents.indices) {
          adjacentMatrix(i, opponentsAlias(j) - 1) = times(j)
        }
      }
    }

    /*    Plotter.setParser(BreezeMwInputParser)
        Plotter.imagesc(users, users, adjacentMatrix)
        Plotter.colorbar()*/

    val path1 = MiubiqConf.expOutPath("data-for-matlab-graph-plot.txt")
    val writer1 = new PrintWriter(new BufferedWriter(new FileWriter(new File(path1))))
    for (i <- 1 until n) {
      for (j <- 0 until i) {
        adjacentMatrix(i, j) = (adjacentMatrix(i, j) + adjacentMatrix(j, i)) / 2.0
        writer1.println(s"${alias(i)}" + "," + s"${users(i)}" + ","
          + s"${alias(j)}" + "," + s"${users(j)}" + "," + adjacentMatrix(i, j))
        writer1.flush()
      }
    }
    writer1.close()

    val path2 = MiubiqConf.expOutPath("users-meta-data.csv")
    val csvWriter = new CSVPrinter(new BufferedWriter(new FileWriter(new File(path2))), CSVFormat.DEFAULT
      .withHeader("BadgeId", "Alias", "Team", "JobType", "JobRole", "kpi1", "kpi2", "kpi3", "kpi4", "kpi5", "kpi6", "kpi7"))

    for (i <- users.indices) {
      val user = users(i)
      csvWriter.printRecord(s"${user}", s"${alias(i)}", s"${metaData.jobGroup.getTeam(user)}",
        s"${metaData.jobType.getJobType(user)}", s"${metaData.jobRole.getJobRole(user)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 1)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 2)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 3)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 4)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 5)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 6)}",
        s"${metaData.getKPI(user, caDataset, raDataset, 7)}"
      )
      csvWriter.flush()
    }

    csvWriter.close()

  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("infraredParser")
      .getOrCreate
  }
}
