/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.miubiq.commons.ml.Helpers._

/**
  * Created by simosaka on 2018/01/06.
  * @param freq Frequency
  * @param xSpectrum Spectrum of X axis
  * @param ySpectrum Spectrum of Y axis
  * @param zSpectrum Spectrum of Z axis
  * @param unixTimeStamp timestamp
  */
case class AccelSpectrum3AxesWithTime(freq: DenVd,
                                      xSpectrum: DenVd,
                                      ySpectrum: DenVd,
                                      zSpectrum: DenVd,
                                      unixTimeStamp: Long)
