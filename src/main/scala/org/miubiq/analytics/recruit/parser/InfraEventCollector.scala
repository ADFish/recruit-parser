/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset

/**
  * Created by duan on 1/16/18.
  * To get face to face communication event from the row infrared sensor data
  */
object InfraEventCollector extends Serializable {

  /**
    * This method collect F2F communication event from raw infrared sensor data
    * @param infraredDataset The raw infrared dataset
    * @return RDD of EventInfo
    */
  def gather(infraredDataset: Dataset[InfraData]): RDD[EventInfo] = {

    infraredDataset.rdd.groupBy(_.otherID)
      .map { row =>
        val otherID = row._1

        val badgeID = row._2.map {
          _.badgeID
        }.head

        val timeArray = row._2.map {
          _.unixTimeStamp
        }.toArray

        EventInfo(
          badgeID,
          otherID,
          timeArray
        )
      }
  }

}
