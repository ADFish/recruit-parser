/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import org.apache.spark.sql.Dataset
import org.miubiq.commons.adapter.BreezeMwInputParser
import org.miubiq.commons.math.brzext.MatFuns._
import org.miubiq.commons.mwplot.Plotter._

/**
  * Created by duan on 12/27/17.
  */
object BluetoothSamplingTimeVisualizer {
  /**
    * This method visualize the sampling time of bluetooth data
    * @param bluetoothDataset The raw bluetooth dataset
    * @return A figure of bluetooth sampling time
    */
  def view(bluetoothDataset: Dataset[BluetoothData]): Unit = {

    val t = bluetoothDataset.sort("unixTimeStamp").
      select("unixTimeStamp").
      collect().
      map(_.getLong(0))

    val n = t.length

    val y = ones(n, 1)

    setParser(BreezeMwInputParser)

    stem(t, y)

    val cal = Calendar.getInstance()
    cal.setTimeZone(TimeZone.getTimeZone("JST"))
    val referenceTime = t.head
    cal.setTimeInMillis(referenceTime)

    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    val beginTime = cal.getTimeInMillis

    val duration: Double = 10000

    val res = math.ceil((t.last - beginTime) / duration).toInt

    val xTick = linspace(beginTime, beginTime + res * duration, res + 1)

    val dateFormat = new SimpleDateFormat("yyyy/MM/dd (E)")
    val dateMassage = dateFormat.format(t.head)

    val timeFormat = new SimpleDateFormat("HH:mm:ss")
    val xTickLabel = xTick.map(time => timeFormat.format(time))

    xlabel(s"$dateMassage")

    set(gca, "XTick", xTick.toArray)
    set(gca, "XTickLabel", xTickLabel.toArray)
  }

}
