/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.analytics.recruit.parser.{KPIParser, ParticipantsPropertyCollector, RoleParser}
import org.miubiq.analytics.recruit.parser.{ParticipantsPropertyCollector, RoleParser}
import org.miubiq.commons.exp.config.parser.ArgConfParser
import org.miubiq.commons.io.MiubiqConf

/**
  * Created by duan on 4/3/18.
  */
object KPILoaderExample {
  def main(args: Array[String]): Unit = {
    val session = startUpSpark()

    val parser = new ArgConfParser(args(0), getClass.getPackage.getName)

    val caFilePath = "testData/recruit/KPIdata_fix_CA.csv"
    val raFilePath = "testData/recruit/KPIdata_fix_RA.csv"
    val path = "testData/recruit/MetaData_Osaka_exported.csv"
    val dataPath = MiubiqConf.inPath("recruit2016/RawDataFromRecruit_ver201701/RawDataFromRecruit/Raw")

    val roleParse = new RoleParser
    val roleDataset = roleParse.loadDataset(session, path)

    val metaData = new ParticipantsPropertyCollector(roleDataset, dataPath)

    val kpiParser = new KPIParser

    val caRawDataset = kpiParser.loadCARawDataset(session, caFilePath)
    val raRawDataset = kpiParser.loadRARawDataset(session, raFilePath)

    val caDataset = kpiParser.loadCADataset(session, caFilePath)
    val raDataset = kpiParser.loadRADataset(session, raFilePath)

    println("Number of people in the CA kpi data file: " + caDataset.collect().length)
    println("Number of people in the RA kpi data file: " + raDataset.collect().length)
    println()

    println("Raw dataset in CA and RA team:")
    caRawDataset.printSchema()
    caRawDataset.show(27)
    raRawDataset.printSchema()
    raRawDataset.show()
    println()

    println("Processed dataset (total kpi from 20150928 to 20151206) in CA and RA team:")
    caDataset.printSchema()
    caDataset.show(27)
    raDataset.printSchema()
    raDataset.show()
    println()

    val teamCA3 = metaData.jobGroup.teamCA3
    val teamCA4 = metaData.jobGroup.teamCA4
    val teamRA2 = metaData.jobGroup.teamRA2
    val teamRA3 = metaData.jobGroup.teamRA3

    val kpiUsers = metaData.participantsIds.filter(id => metaData.hasKPIData(id))

    val kpiType: Int = 5


    println("---------------TeamCA3G members' kpi-----------------")
    println("TeamCA3G total number of people: " + teamCA3.size)
    println(s"-------KPI$kpiType-------")
    println(s"TeamCA3G number of people whoes KPI$kpiType unequals 0: " +
      teamCA3.count { id =>
        metaData.hasKPIData(id) &&
          metaData.getKPI(id, caDataset, raDataset, kpiType) != 0 &&
          !metaData.getKPI(id, caDataset, raDataset, kpiType).isNaN &&
          metaData.fileGetter.getAccelFiles(id).size > 1
      }
    )

    var teamCA3KPI = Array.empty[Double]
    teamCA3.foreach { id =>
      teamCA3KPI :+= metaData.getKPI(id, caDataset, raDataset, kpiType)
    }

    val kpiCA3 = teamCA3KPI.filter { it =>
      !it.isNaN && it != 0
    }
    val kpiAvgCA3 = kpiCA3.sum / kpiCA3.length
    val kpiVarCA3 = kpiCA3.map { it =>
      math.pow((it - kpiAvgCA3), 2)
    }.sum / (kpiCA3.length - 1.0)
    val kpiMaxCA3 = kpiCA3.max
    val kpiMinCA3 = kpiCA3.min
    println(s"Average KPI$kpiType in CA3 team (exclude 0): " + kpiAvgCA3)
    println(s"Variance KPI$kpiType in CA3 team (exclude 0): " + kpiVarCA3)
    println(s"Max KPI$kpiType in CA3 team(exclude 0) :" + kpiMaxCA3)
    println(s"Min KPI$kpiType in CA3 team(exclude 0) :" + kpiMinCA3)
    println()

    
    println("---------------TeamCA4G members' kpi-----------------")
    println("TeamCA4G total number of people: " + teamCA4.size)
    println(s"-------KPI$kpiType-------")
    println(s"TeamCA4G number of people whoes KPI$kpiType unequals 0: " +
      teamCA4.count { id =>
        metaData.hasKPIData(id) &&
          metaData.getKPI(id, caDataset, raDataset, kpiType) != 0 &&
          !metaData.getKPI(id, caDataset, raDataset, kpiType).isNaN &&
          metaData.fileGetter.getAccelFiles(id).size > 1
      }
    )

    var teamCA4KPI = Array.empty[Double]
    teamCA4.foreach { id =>
      teamCA4KPI :+= metaData.getKPI(id, caDataset, raDataset, kpiType)
    }

    val kpiCA4 = teamCA4KPI.filter { it =>
      !it.isNaN && it != 0
    }
    val kpiAvgCA4 = kpiCA4.sum / kpiCA4.length
    val kpiVarCA4 = kpiCA4.map { it =>
      math.pow((it - kpiAvgCA4), 2)
    }.sum / (kpiCA4.length - 1.0)
    val kpiMaxCA4 = kpiCA4.max
    val kpiMinCA4 = kpiCA4.min
    println(s"Average KPI$kpiType in CA4 team (exclude 0): " + kpiAvgCA4)
    println(s"Variance KPI$kpiType in CA4 team (exclude 0): " + kpiVarCA4)
    println(s"Max KPI$kpiType in CA4 team(exclude 0) :" + kpiMaxCA4)
    println(s"Min KPI$kpiType in CA4 team(exclude 0) :" + kpiMinCA4)
    println()
    
    
    println("---------------TeamRA2G members' kpi-----------------")
    println("TeamRA2G total number of people: " + teamRA2.size)
    println(s"-------KPI$kpiType-------")
    println(s"TeamRA2G number of people whoes KPI$kpiType unequals 0: " +
      teamRA2.count { id =>
        metaData.hasKPIData(id) &&
          metaData.getKPI(id, caDataset, raDataset, kpiType) != 0 &&
          !metaData.getKPI(id, caDataset, raDataset, kpiType).isNaN &&
          metaData.fileGetter.getAccelFiles(id).size > 1
      }
    )

    var teamRA2KPI = Array.empty[Double]
    teamRA2.foreach { id =>
      teamRA2KPI :+= metaData.getKPI(id, caDataset, raDataset, kpiType)
    }

    val kpiRA2 = teamRA2KPI.filter { it =>
      !it.isNaN && it != 0
    }
    val kpiAvgRA2 = kpiRA2.sum / kpiRA2.length
    val kpiVarRA2 = kpiRA2.map { it =>
      math.pow((it - kpiAvgRA2), 2)
    }.sum / (kpiRA2.length - 1.0)
    val kpiMaxRA2 = kpiRA2.max
    val kpiMinRA2 = kpiRA2.min
    println(s"Average KPI$kpiType in RA2 team (exclude 0): " + kpiAvgRA2)
    println(s"Variance KPI$kpiType in RA2 team (exclude 0): " + kpiVarRA2)
    println(s"Max KPI$kpiType in RA2 team(exclude 0) :" + kpiMaxRA2)
    println(s"Min KPI$kpiType in RA2 team(exclude 0) :" + kpiMinRA2)
    println()


    println("---------------TeamRA3G members' kpi-----------------")
    println("TeamRA3G total number of people: " + teamRA3.size)
    println(s"-------KPI$kpiType-------")
    println(s"TeamRA3G number of people whoes KPI$kpiType unequals 0: " +
      teamRA3.count { id =>
        metaData.hasKPIData(id) &&
          metaData.getKPI(id, caDataset, raDataset, kpiType) != 0 &&
          !metaData.getKPI(id, caDataset, raDataset, kpiType).isNaN &&
          metaData.fileGetter.getAccelFiles(id).size > 1
      }
    )

    var teamRA3KPI = Array.empty[Double]
    teamRA3.foreach { id =>
      teamRA3KPI :+= metaData.getKPI(id, caDataset, raDataset, kpiType)
    }

    val kpiRA3 = teamRA3KPI.filter { it =>
      !it.isNaN && it != 0
    }
    val kpiAvgRA3 = kpiRA3.sum / kpiRA3.length
    val kpiVarRA3 = kpiRA3.map { it =>
      math.pow((it - kpiAvgRA3), 2)
    }.sum / (kpiRA3.length - 1.0)
    val kpiMaxRA3 = kpiRA3.max
    val kpiMinRA3 = kpiRA3.min
    println(s"Average KPI$kpiType in RA3 team (exclude 0): " + kpiAvgRA3)
    println(s"Variance KPI$kpiType in RA3 team (exclude 0): " + kpiVarRA3)
    println(s"Max KPI$kpiType in RA3 team(exclude 0) :" + kpiMaxRA3)
    println(s"Min KPI$kpiType in RA3 team(exclude 0) :" + kpiMinRA3)
    println()

  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("recruitParser")
      .getOrCreate
  }
}
