/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.Dataset

/**
  * Created by duan on 2/13/18.
  */

class ParticipantsPropertyCollector(val roleDataset: Dataset[RoleData], dataPath: String) extends Serializable {

  val participantsRoleDataset: Dataset[RoleData] = roleDataset.filter { it =>
    it.participation == "Participation" &&
      it.dataDirectory != "Checking"
  }

  //get all participants' badge Ids
  val participantsIds: Array[Int] = participantsRoleDataset.rdd.map {
    _.badgeId
  }.collect()
    .sorted

  /**
    * This method judge the user is a participant or not
    * @param id User id
    * @return True if the user is participants
    */
  def isParticipants(id: Int): Boolean = {
    participantsIds.contains(id)
  }

  //alias is useful for social network plotting
  private val participantsAliasMap = participantsIds.map { it =>
    (it, participantsIds.indexOf(it) + 1)
  }.toMap

  /**
    * This method get the alias id of the participant
    * @param id User id
    * @return The alias of the User, for example the alias of user 2850 is 1, 2851 is 2 etc.
    */
  def getParticipantsAlias(id: Int): Int = {
    participantsAliasMap(id)
  }

  /**
    * This method judge whether the user has KPI or not
    * @param id User id
    * @return True if the user has KPI
    */
  def hasKPIData(id: Int): Boolean = {
    KPIDataMap(id) == "w"
  }

  private val KPIDataMap = participantsRoleDataset.rdd.map { row =>
    (row.badgeId, row.KPIData)
  }.collect
    .toMap


  val jobGroup = new JobGroup(participantsRoleDataset)

  val jobType = new JobType(participantsRoleDataset)

  val jobRole = new JobRole(participantsRoleDataset)

  val fileGetter = new FileGetter(participantsRoleDataset, dataPath)

  def beginDate = fileGetter.beginDate

  def endDate = fileGetter.endDate

  /**
    * This method get the given kind of KPI of a given user
    * @param id User's id
    * @param caKPIDataset The KPI dataset of CA team
    * @param raKPIDataset The kPI dataset of RA team
    * @param kpiKind The kind of KPI
    * @return KPI value of the given user
    */
  def getKPI(id: Int, caKPIDataset: Dataset[KPICAData], raKPIDataset: Dataset[KPIRAData], kpiKind: Int): Double = {
    val kPIGetter = new KPIGetter
    kPIGetter.getKPI(id, caKPIDataset, raKPIDataset, kpiKind)
  }

}
