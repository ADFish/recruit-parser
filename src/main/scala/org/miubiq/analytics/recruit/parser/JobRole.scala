/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.Dataset

/**
  * Note:Job Role of "General affairs", "General Manager" and "Staff for a specific project" don't have KPI.
  *
  * Created by duan on 2/13/18.
  */
class JobRole(val roleDataset: Dataset[RoleData]) extends Serializable {

  val staff = "Sales Staff "
  val affair = "General affairs"
  val manager = "General Mgr"
  val other = "Staff for a specific project "

  private val groupUsersByJobRole: Map[String, Set[Int]] = {
    roleDataset.rdd.groupBy(_.jobRole).
      map {
        case (jobRole, roleData) =>
          (jobRole, roleData.map(_.badgeId).toSet)
      }.
      collect.
      toMap
  }

  val staffEmployees: Set[Int] = groupUsersByJobRole(staff)
  val affairEmployees: Set[Int] = groupUsersByJobRole(affair)
  val managerEmployees: Set[Int] = groupUsersByJobRole(manager)
  val otherEmployees: Set[Int] = groupUsersByJobRole(other)

  /**
    * Get the job role of a user
    * @param id User's id
    * @return job role name
    */
  def getJobRole(id: Int): String = {
    if (staffEmployees.contains(id)) {
      "Sales Staff"
    } else if (affairEmployees.contains(id)) {
      affair
    } else if (managerEmployees.contains(id)) {
      manager
    } else if (otherEmployees.contains(id)) {
      "Staff for a specific project"
    } else {
      "no-data"
    }
  }

}
