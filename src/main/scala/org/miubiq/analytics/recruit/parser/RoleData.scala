/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

/**
  * Created by simosaka on 2018/01/19.
  */

/**
  *
  * @param personalId user alias
  * @param participation Is a participation or not participation
  * @param badgeId user id
  * @param dataDirectory directory for saving raw sensor data
  * @param team Team
  * @param jobType Job type
  * @param jobRole Job role
  * @param KPIData KPI data
  */
case class RoleData(
                     personalId: Int,
                     participation: String,
                     badgeId: Int,
                     dataDirectory: String,
                     team: String,
                     jobType: Int,
                     jobRole: String,
                     KPIData: String
                   )