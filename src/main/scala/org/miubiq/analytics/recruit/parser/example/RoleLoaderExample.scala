/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.miubiq.analytics.recruit.parser.RoleParser
import org.miubiq.analytics.recruit.parser.{ParticipantsPropertyCollector, RoleParser}
import org.miubiq.commons.io.MiubiqConf

/**
  * Created by duan on 1/19/18.
  */
object RoleLoaderExample {

  def main(args: Array[String]): Unit = {

    val session = startUpSpark()

    val path = "testData/recruit/MetaData_Osaka_exported.csv"

    val roleParse = new RoleParser
    val roleDataset = roleParse.loadDataset(session, path)

    val dataPath = MiubiqConf.inPath("recruit2016/RawDataFromRecruit_ver201701/RawDataFromRecruit/Raw")

    val metaData = new ParticipantsPropertyCollector(roleDataset, dataPath)
    metaData.participantsRoleDataset.show()

    println(s"Number of Participants: ${metaData.participantsIds.length}")
    println()

    println("------Perticipants in each Team--------")
    val jobGroup = metaData.jobGroup
    println(s"RA_2G Team Members: ${jobGroup.teamRA2.mkString(" ")}")
    println(s"RA_3G Team Members: ${jobGroup.teamRA3.mkString(" ")}")
    println(s"CA_3G Team Members: ${jobGroup.teamCA3.mkString(" ")}")
    println(s"CA_4G Team Members: ${jobGroup.teamCA4.mkString(" ")}")
    println(s"Other team Members: ${jobGroup.otherTeam.mkString(" ")}")
    println()

    println("------Perticipants of different Role--------")
    val jobRole = metaData.jobRole
    println(s"${jobRole.staff}: ${jobRole.staffEmployees.mkString(" ")}")
    println(s"${jobRole.affair}: ${jobRole.affairEmployees.mkString(" ")}")
    println(s"${jobRole.manager}: ${jobRole.managerEmployees.mkString(" ")}")
    println(s"${jobRole.other}: ${jobRole.otherEmployees.mkString(" ")}")
    println()

    println("------Perticipants of different job types--------")
    val jobType = metaData.jobType
    println(s"${jobType.permanent}: ${jobType.permanentEmployees.mkString(" ")}")
    println(s"${jobType.limited}: ${jobType.limitedContractEmployees.mkString(" ")}")
    println(s"${jobType.temporary}: ${jobType.temporaryEmployees.mkString(" ")}")
    println(s"${jobType.other}: ${jobType.otherEmployees.mkString(" ")}")
    println()

    println("User2850's infrared files:")
    println(metaData.fileGetter.getInfraredFiles(2850).mkString(","))
    println()
    println("User2850's infrared file on 20151026:")
    println(metaData.fileGetter.getInfraredFile(2850, "20151026"))

    println()
    println("User2850's accelerometer files:")
    println(metaData.fileGetter.getAccelFiles(2850).mkString(","))
    println()
    println("User2850's accelerometer file on 20151026:")
    println(metaData.fileGetter.getAccelFile(2850, "20151026"))

    println()
    println("User2850's bluetooth files:")
    val bluetoothFiles = metaData.fileGetter.getBluetoothFiles(2850)
    println(bluetoothFiles.mkString(","))
    println()
    println("User2850's bluetooth file on 20151026:")
    println(metaData.fileGetter.getBluetoothFile(2850, "20151026"))

    println()
    println("User 2850's attendance day:")
    val days = bluetoothFiles.map(metaData.fileGetter.getDate(_))
    println(days.mkString(","))

  }

  def startUpSpark(): SparkSession = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    SparkSession.builder
      .master("local[*]")
      .appName("roleParser")
      .getOrCreate
  }
}
