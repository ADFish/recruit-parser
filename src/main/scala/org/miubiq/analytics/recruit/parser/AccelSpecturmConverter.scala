/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.transform.{DftNormalization, FastFourierTransformer, TransformType}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset
import org.miubiq.commons.ml.Helpers.DenVd
import org.miubiq.commons.ml.util.TimeSeriesSlidingWindow


/**
  * Created by duan on 1/2/18.
  */
object AccelSpecturmConverter extends Serializable {

  // we assume the base sampling rate is 2 [Hz] and is fixed.
  private val samplingInterval = 0.5 // in seconds

  private def comp2Norm(x: Complex): Double = {
    val real = x.getReal
    val imag = x.getImaginary
    math.sqrt(real * real + imag + imag)
  }


  private def fft(data: Array[Double], samplingInterval: Double): AccelSpectrum = {

    require(samplingInterval > 0, "sampling interval [s] should be positive")

    val transformer = new FastFourierTransformer(DftNormalization.STANDARD)
    val transform = transformer.transform(data, TransformType.FORWARD)

    val spectrumSize = data.length / 2

    val amplitude = transform.take(spectrumSize).map(
      f => comp2Norm(f)
    )

    val pow = new DenVd(amplitude)

    val freq = new DenVd(spectrumSize)
    val maxFreq = 1.0 / samplingInterval / 2.0

    for (i <- 0 until spectrumSize) {
      freq(i) = maxFreq * i / spectrumSize
    }

    AccelSpectrum(freq, pow)
  }

  //copy from
  // https://stackoverflow.com/questions/9146855/determine-if-num-is-a-power-of-two-in-java
  private def isExponent2(x: Int): Boolean = {
    ((x & -x) == x)
  }

  /**
    * This method generate the accelerometer spectrum with timestamp
    * @param accelDataset The row accelerometer dataset
    * @param windowSize The window size
    * @param sliding The sliding size
    * @return A rdd of AccelSpectrum3AxesWithTime
    */
  def get(accelDataset: Dataset[AccelData],
          windowSize: Int,
          sliding: Int):
  RDD[AccelSpectrum3AxesWithTime] = {

    require(isExponent2(windowSize), "windowSize must be power of 2")

    require(sliding > 0, "sidling must be positive")


    val n = accelDataset.count()

    val thing = 0

    // we need to choose odd number of samples rather than evan number.
    // argument windowSize in convert method must be odd number
    val slidingRDD = TimeSeriesSlidingWindow.convert(
      accelDataset.rdd,
      windowSize + 1,
      thing,
      sliding
    )

    slidingRDD.map {
      accelArray => {
        val fftTarget = accelArray.slice(0, windowSize)

        val xArray = fftTarget.map { it => it.x }
        val xSpectrum = fft(xArray, samplingInterval)

        val yArray = fftTarget.map { it => it.y }
        val ySpectrum = fft(yArray, samplingInterval)

        val zArray = fftTarget.map { it => it.z }
        val zSpectrum = fft(zArray, samplingInterval)

        val freq = xSpectrum.freq

        val center = (windowSize + 1) / 2

        AccelSpectrum3AxesWithTime(freq,
          xSpectrum.pow,
          ySpectrum.pow,
          zSpectrum.pow,
          accelArray(center).unixTimeStamp)
      }
    }
  }
}
