/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset

import scala.collection.mutable

/**
  * This creates collection of fingerprints from bluetooth scan dataset.
  *
  * Created by duan on 1/10/18.
  *
  * @param millisec segmentation threshold (duration between 2 samples are larger
  *                 than millisec, this builder make segmentation and create fingerprints respectively.
  */
class BluetoothFingerprintBuilder(millisec: Long = 4000) extends Serializable {

  require(millisec > 500 && millisec < 25 * 1000, s"millisec should be [500, 25000]")

  /**
    * This method build fingerprint from raw bluetooth dataset
    * @param bluetoothDataset The raw bluetooth datast
    * @return An array of FingerprintWithTimeInfo
    */
  def getFingerprint(bluetoothDataset: Dataset[BluetoothData]): Array[FingerprintWithTimeInfo] = {

    val btWithTagID = getBtWithTagID(bluetoothDataset)

    val chunks = combineByTagID(btWithTagID)
    chunks.map(convert).collect().sortBy(_.unixTimestamp)
  }

  private def convert(btArray: Array[BtWithSegTagID]): FingerprintWithTimeInfo = {

    val unixTimestamp = btArray.map(_.unixTimeStamp).min

    val fingerPrint = new mutable.HashMap[String, Double]

    for (i <- btArray.indices) {
      fingerPrint += (btArray(i).otherBt -> btArray(i).rssi)
    }

    FingerprintWithTimeInfo(
      Fingerprint(fingerPrint.toMap),
      unixTimestamp
    )
  }

  private case class BtWithSegTagID(
                                     badgeID: Int,
                                     unixTimeStamp: Long,
                                     otherBt: String,
                                     btClass: String,
                                     rssi: Int,
                                     segTag: Int
                                   )


  private def combineByTagID(btWithSegTagIdRDD: RDD[BtWithSegTagID]): RDD[Array[BtWithSegTagID]] = {
    val init = (value: BtWithSegTagID) => {
      List(value)
    }

    val combiner = (aggr: List[BtWithSegTagID], value: BtWithSegTagID) => {
      value :: aggr
    }
    val merger = (aggr1: List[BtWithSegTagID], aggr2: List[BtWithSegTagID]) => {
      aggr1 ::: aggr2
    }

    btWithSegTagIdRDD.keyBy(_.segTag).
      combineByKey(init, combiner, merger).map(_._2.toArray)
  }

  private def getBtWithTagID(bluetoothDataset: Dataset[BluetoothData]): RDD[BtWithSegTagID] = {

    val btRDD = bluetoothDataset.rdd.sortBy(_.unixTimeStamp)

    val tagIdRDD = getSegmentationID(btRDD)

    val btWithTagIdRDD = btRDD.coalesce(1).zip(tagIdRDD.coalesce(1))

    // convert tuple into case class
    btWithTagIdRDD.map {
      case (bt, tagId) => {
        BtWithSegTagID(
          badgeID = bt.badgeID,
          unixTimeStamp = bt.unixTimeStamp,
          otherBt = bt.otherBt,
          btClass = bt.btClass,
          rssi = bt.rssi,
          segTag = tagId)
      }
    }
  }

  /*
   * unixTimeStamp: t1,       t2, t3, t4, t5,    t6,     t7,     t8, t9, t10,    t11, t12
   * tag:            E,       B,   I,  I,  E,     S,      S,      B,  I,   E,      B,   I,...
   * segID:          3,       4 ,  4,  4,  4,     5,      6,      7,  7,   7,      8,   8,...
   * @todo this cannot ensure maximum size of chunks. If millisec is large, this could not properly segment input seq.
   */
  private def getSegmentationID(btRDD: RDD[BluetoothData]): RDD[Int] = {

    val tsArray = btRDD.map(_.unixTimeStamp).collect()

    val n = tsArray.length
    val segInfo = new Array[String](n)

    for (t <- 1 until n - 1) {
      val input = Array(tsArray(t - 1), tsArray(t), tsArray(t + 1))
      segInfo(t) = assignTag(input)
    }

    val diff0 = tsArray(1) - tsArray(0)

    if (diff0 > millisec) {
      segInfo(0) = "S"
    }

    val diffE = tsArray(n - 1) - tsArray(n - 2)

    if (diffE > millisec) {
      segInfo(n - 1) = "S"
    }

    val labelArray = new Array[Int](n)

    var labelId = 0
    for (t <- 0 until n) {
      if (segInfo(t) == "B" || segInfo(t) == "S") {
        labelId += 1
      }

      labelArray(t) = labelId
    }

    // this part is for debugging to tune hyper parameters.
    // this also should be erased in this class but add the last time into fingerprint
    /*var current = 0
    var scanSize = 0
    var beginTime = 0L
    var numSingleSeg = 0
    for (t <- 0 until n) {
      if (segInfo(t) == "S") {
        numSingleSeg += 1
      }
      if (current < labelArray(t)) {
        if (current > 0) {
          println(s"number of scan at $current: : $scanSize")
        }
        scanSize = 0
        current = labelArray(t)
      } else {
        scanSize += 1
      }

      if (segInfo(t) == "B") {
        beginTime = tsArray(t)
      }
      if (segInfo(t) == "E") {
        println(s"segment duration at $current: ${(tsArray(t) - beginTime) / 1000.0} [s] ")
      }
    }
    println(s"num of S point: $numSingleSeg")*/


    btRDD.sparkContext.parallelize(labelArray)
  }


  /*
   * allocate tag information like nlp
   * B: Begin
   * E: End
   * I: Inside
   * S: Single
   */
  private def assignTag(ts: Array[Long]): String = {
    val diff1 = ts(1) - ts(0)
    val diff2 = ts(2) - ts(1)

    val threshold = millisec
    if (diff1 > threshold && diff2 < threshold) {
      "B"
    } else if (diff1 < threshold && diff2 > threshold) {
      "E"
    } else if (diff1 > threshold && diff2 > threshold) {
      "S"
    } else {
      "I"
    }
  }
}

