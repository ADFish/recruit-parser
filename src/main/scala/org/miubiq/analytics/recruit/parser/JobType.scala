/*
 * Copyright 2018 Shimosaka Research Group, Tokyo Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.miubiq.analytics.recruit.parser

import org.apache.spark.sql.Dataset

/**
  * Contract Type: 1, permanent employee; 2, employee under limited-term contract;
  * 3, temporary worker from a agency; 4, others
  *
  * NOTE: Person of Job Type 3 and 4 don't have KPI.
  *
  * Created by duan on 2/13/18.
  */
class JobType(val roleDataset: Dataset[RoleData]) extends Serializable {

  val permanent = "permanent employee"
  val limited = "employee under limited-term contract"
  val temporary = "temporary worker from a agency"
  val other = "others"

  private val groupUsersByJobType: Map[Int, Set[Int]] = {
    roleDataset.rdd.groupBy(_.jobType).
      map {
        case (jobType, roleData) =>
          (jobType, roleData.map(_.badgeId).toSet)
      }.
      collect.
      toMap
  }

  val permanentEmployees: Set[Int] = groupUsersByJobType(1)
  val limitedContractEmployees: Set[Int] = groupUsersByJobType(2)
  val temporaryEmployees: Set[Int] = groupUsersByJobType(3)
  val otherEmployees: Set[Int] = groupUsersByJobType(4)

  /**
    * Get job type name of a user
    * @param id user's id
    * @return job type name
    */
  def getJobTypeName(id: Int): String = {
    if (permanentEmployees.contains(id)) {
      permanent
    } else if (limitedContractEmployees.contains(id)) {
      limited
    } else if (temporaryEmployees.contains(id)) {
      temporary
    } else if (otherEmployees.contains(id)) {
      other
    } else {
      "no-data"
    }
  }

  /**
    * Get job type id of a user
    * @param id User's id
    * @return Job type id
    */
  def getJobType(id: Int): Int = {
    if (permanentEmployees.contains(id)) {
      1
    } else if (limitedContractEmployees.contains(id)) {
      2
    } else if (temporaryEmployees.contains(id)) {
      3
    } else if (otherEmployees.contains(id)) {
      4
    } else {
      0
    }
  }

}
